/**
 * @file Display_btClient_LinkySimulator.ino
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-07-20
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <Arduino.h>
#include <U8g2lib.h>

//BT includes
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>

//Display definitions
#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif
#define SDA_PIN 5
#define SCL_PIN 6

//BLE definitions
// See the following for generating UUIDs:
// https://www.uuidgenerator.net/
#define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"

U8G2_SSD1306_72X40_ER_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);   // EastRising 0.42" OLED

String data;
char dataChar[256];
BLECharacteristic *pCharacteristic;
int connected = 0;

class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string value = pCharacteristic->getValue();
      String dataStr;

      if (value.length() > 0) {
        Serial.println("*********");
        Serial.print("New value: ");
        memset(dataChar, 0, 256);
        for (int i = 0; i < value.length(); i++) {
          Serial.print(value[i]);
          dataChar[i] += value[i];
        }
          

        Serial.println();
        Serial.println("*********");
        //data = value;
      }
    }
};

class myServerCallbacks: public BLEServerCallbacks {
  
  void onConnect(BLEServer *pServer)
  {
    connected = 1;
    Serial.println("DEVICE CONNECTED");
  }

  void onDisconnect(BLEServer *pServer)
  {
    connected = 0;
    Serial.println("DEVICE DISCONNECTED");
  }

};

void setup ()
{
  Wire.begin(SDA_PIN, SCL_PIN);
  u8g2.begin();
  data = "Hello World";
  memset(dataChar, 0, 256);
  data.toCharArray(dataChar, data.length());

  Serial.begin(9600);

  Serial.println("1- Download and install an BLE scanner app in your phone");
  Serial.println("2- Scan for BLE devices in the app");
  Serial.println("3- Connect to MyESP32");
  Serial.println("4- Go to CUSTOM CHARACTERISTIC in CUSTOM SERVICE and write something");
  Serial.println("5- See the magic =)");

  BLEDevice::init("noWatt");
  BLEServer *pServer = BLEDevice::createServer();
  
  pServer->setCallbacks(new myServerCallbacks());

  BLEService *pService = pServer->createService(SERVICE_UUID);

  pCharacteristic = pService->createCharacteristic(
                                         CHARACTERISTIC_UUID,
                                         BLECharacteristic::PROPERTY_READ |
                                         BLECharacteristic::PROPERTY_WRITE |
                                         BLECharacteristic::PROPERTY_NOTIFY
                                       );

  pCharacteristic->setCallbacks(new MyCallbacks());

  pCharacteristic->setValue("Hello World");
  pService->start();

  //BLEAdvertising *pAdvertising = pServer->getAdvertising();
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
  pAdvertising->setMinPreferred(0x12);
  //BLEDevice::startAdvertising();

  //pAdvertising->start();
}

int counter = 0;

void loop()
{
  u8g2.clearBuffer();					// clear the internal memory
  u8g2.setFont(u8g2_font_ncenB08_tr);	// choose a suitable font
  u8g2.drawStr(0,10, dataChar);	// write something to the internal memory
  u8g2.sendBuffer();					// transfer internal memory to the display

  String data = "value :";
  data += counter++;

  

  if (connected) {
    pCharacteristic->setValue(data.c_str());
    pCharacteristic->notify();
  } else {
        delay(250);
    /**
     * @brief Construct a new BLEDevice::startAdvertising object
     * For the first connecttion advertising must bee initialized.
     * If a connected devices disconnects, advertising must be restarted this way
     * 
     */
    BLEDevice::startAdvertising();
  }

  if (connected == 0) {

  }

  delay(250);

}

void serialEvent()
{
  String data;

  while(Serial.available()) {
    data += (char)Serial.read();
  }

  //pCharacteristic->writeValue(data.c_str());
}