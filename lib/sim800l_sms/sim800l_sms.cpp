/**
 * @file sim800l_sms.cpp
 * @author Martin Acosta (martin.acostaec@gmail.com)
 * @brief 
 * 
 * Library based on the following respositories:
 * 	- https://github.com/vittorioexp/Sim800L-Arduino-Library-revised
 *  - https://github.com/vshymanskyy/TinyGSM
 * 
 * Bassed on the master library 
 * @version 0.1
 * @date 2022-07-01
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "sim800l_sms.h"
#include <string.h>

#define WAIT_OK 1


SIM800L_SMS::SIM800L_SMS() 
{
	_sim800l_initialized = 0;
	_sms_count = 0;
	_setup_state = 0;
	_deleteSms_state = 0;
	_send_state = 0;
	_read_state = 0;
	_smsToSend = 0;
	_buffer = "";
}

SIM800L_SMS::~SIM800L_SMS()
{

}

/**
 * @brief flush rx buffer in serial port
 * This is a blocking function.
 * 
 */
void SIM800L_SMS::flush_serial()
{
	int tmp;
	
	if (_gsm_serial->available() == 0)
		return;


	while (_gsm_serial->available()) {
		tmp = _gsm_serial->read();
	}
}

/**
 * @brief Read data in the RX buffer from the class serial port
 * it waits for the timeout time (MIN 500ms)  for any data.
 * if waitOK is true, it will jump to the begin of the function (after the
 * declaration of the containes string of the data) to continue reading 
 * the input data until having the OK string in the buffer.
 * In case the OK string is not recived, the timeout will be reach
 * and function will return a empty string.
 * 
 * @param timeout Default 1000ms, MIN 500ms
 * @param waitOk Waits for the OK string sent by the module
 * @return String 
 */
int SIM800L_SMS::read_serial(const char *waitFor = "OK")
{
	/*Data recived, read full buffer*/
	while(_gsm_serial->available()) {
		_buffer += (char)_gsm_serial->read();
	}

	/*Waits for the OK string to be preent in the buffer*/
	if (_buffer.indexOf(waitFor) == -1)
		return 0;

	return 1;
}

/**
 * @brief ONLY FOR DEBUG
 * 
 * @param buf1 Char buffer. Usualy it is a manual text
 * @param buf2 
 * @param buf3 
 * @param buf4 
 * @return int 
 */
int SIM800L_SMS::print_serial(char *buf1, String buf2 = "", String buf3 = "", String buf4 = "")
{
	Serial.print("[SIM800L] - ");
	Serial.print(buf1);
	Serial.print(buf2);
	Serial.print(buf3);
	Serial.println(buf4);
}

/**
 * @brief Initializes serial comunications and configurates the modules 
 * to listen to SMS.
 *  - Initializes serial port given the RX and TX pins. Here we use a pointer
 *    to a HardwareSerial object created by the user in the main aplication
 *  - Check if communication OK by sending AT command
 *  - Set the text mode for sms actions
 *  - Set GSM 7bit default
 *  - Deletes all messages
 * 
 * We are force to use the library HardwareSerial to remap the RX & TX pins
 * dans d'autres GPIO's
 * 
 * @return int 0 on succes. 
 *  -1 after 10s timout error
 */
int SIM800L_SMS::setup(HardwareSerial *newSerial, int baud, int rx_pin, int tx_pin)
{
	int count = 0;
	int res;

	if (_setup_state == 0) {
		//Set serial 
		_gsm_serial = newSerial;

		if (!newSerial)
			print_serial("ERROR - Must pass a pointer to a serial object");

		_gsm_serial->begin(baud, SERIAL_8N1, rx_pin, tx_pin);
		if (!_gsm_serial) {
			print_serial("ERROR - newSerial->begin() failed");
			_gsm_serial = NULL; //Reset Default Value
			return -1;
		}

		_setup_state++;

		/*flush serial to clear buffer before sending any commands*/
		flush_serial();

	} else if (_setup_state == 1) {
		/*Check communication between uC and SIM800L*/
		_gsm_serial->print("AT\r\n");
		_setup_state++;
	} else if (_setup_state == 2) {
		read_serial();

		if (_buffer.endsWith("OK\r\n") == 1) { //If false. Answer not OK
			_buffer = "";
			_setup_state++;
			_timeout_count = 0;
		} else {
			_timeout_count++;
			if (_timeout_count > TIMEOUT_COUNT_LIMIT) {
				_timeout_count = 0;
				_setup_state--;
			}
		}
	} else if (_setup_state == 3) {
		/*In this point, module communicates with ESP32*/
		/*Next step is to check if sim card is ready for use*/

		// Set preferrend message format to text mode
		_gsm_serial->print("AT+CMGF=1\r\n");

		_setup_state++;
	} else if (_setup_state == 4) {
		read_serial();

		if (_buffer.endsWith("OK\r\n") == 1) { //If false. Answer not OK
			_buffer = "";
			_setup_state++;
			_timeout_count = 0;
		} else {
			_timeout_count++;
			if (_timeout_count > TIMEOUT_COUNT_LIMIT) {
				_timeout_count = 0;
				_setup_state--;
			}
		}
	} else if (_setup_state == 5) {
		//Set GSM 7bit default alphabet (3GPP TS 23.038)
		_gsm_serial->print("AT+CSCS=\"GSM\"\r\n");

		_setup_state++;
	} else if (_setup_state == 6) {
		read_serial();

		if (_buffer.endsWith("OK\r\n") == 1) { //If false. Answer not OK
			_buffer = "";
			_setup_state++;
			_timeout_count = 0;
		} else {
			_timeout_count++;
			if (_timeout_count > TIMEOUT_COUNT_LIMIT) {
				_timeout_count = 0;
				_setup_state--;
			}
		}
	} else if (_setup_state == 7) {
		if (SMS.delete_all_SMS() == -1) {
			Serial.println("Error deleting SMS");
		}

		_setup_state++;
	} else {
		if (_sim800l_initialized) {
			print_serial("ERROR - Object already initialized");
		}
		_sim800l_initialized = 1;
		return 1;
	}

	return 0;
}

/**
 * @brief Is there any not read message available
 * The module SIM800L sends the CMTI command when it recives an
 * SMS. This operations is not blocking and must be performed périodically
 * 
 * @return int 1 if message available, 0 if not 
 */
int SIM800L_SMS::available()
{
	String nbSmsString;

	read_serial();
	if (_buffer.length() == 0) 
		return 0;

	if (_buffer.indexOf("CMTI") == -1)
		return 0;

	/**
	 * @brief Get the index of the sms to read
	 * syntax of answer:
	 *    +CMTI_"SM",1
	 * 
	 */
	nbSmsString = _buffer.substring(_buffer.indexOf(',') + 1);

	_buffer = "";

	return nbSmsString.toInt();
	
}

/**
 * @brief Read SMS if available.
 * 
 * @param sender User char array to save sender number
 * @param buf User char array to save data (160 char max)
 * @return int number of readed bytes
 *  -1 if error (not ant message available)
 */
int SIM800L_SMS::read(String& sender, String& buf)
{
	int count;
	int curByte;
	int index;
	int first;
	int second;
	int thirt;

	switch (_read_state)
	{
	case 0:

		index = available();

		if (!index || _sim800l_initialized == 0)
			return -1;

		_gsm_serial->print("AT+CMGR=");
		_gsm_serial->print(index);
		_gsm_serial->print("\r\n");

		_read_state++;
		break;
	case 1:
		read_serial();

		if (_buffer.endsWith("OK\r\n") != -1) {
			_read_state++;

			/**
			 * @brief Read message
			 *  reading message syntax:
			 *    +CMGR:"REC UNREAD"
			 *    "+33722110090","","02/01/30,20:40:31+00"
			 *    Content of the sms
			 * 
			 *    OK
			 * 
			 */
			first 	= _buffer.indexOf('\n', 2) + 1;
			second 	= _buffer.indexOf('\n', first) + 1;
			thirt	= _buffer.indexOf('\n', second);
			buf 		= _buffer.substring(second, thirt);

			if (_buffer.length() > 10) {//avoid empty sms
					uint8_t _idx1	=_buffer.indexOf("+CMGR:");
					_idx1		=_buffer.indexOf("\",\"", _idx1+1);
					sender	= _buffer.substring(_idx1+3,_buffer.indexOf("\",\"",_idx1+4));
			} else {
					sender = "";
			}

			_sms_count++;

			_buffer = "";
			_read_state++;
			_timeout_count = 0;
			return 1;
		} else {
			_timeout_count++;
			if (_timeout_count > TIMEOUT_COUNT_LIMIT) {
				_timeout_count = 0;
				_read_state--;
			}
		}
		break;
	default:
		_read_state = 0;
		break;
	}

	return 0;
}


/**
 * @brief Private function called internaly. It handles the state machine to
 * send a programmed message.
 * 
 * @return int 	0 -> noting to send
 * 				1 -> SMS sent succesfully
 * 				-1 -> error
 */
int SIM800L_SMS::send()
{
	String temp;
	char *tmpStateCmgs = "CMGS";

	if (_smsToSend == 0)
		return 0;
	
	if (_sim800l_initialized == 0) {
		print_serial("ERROR - befor sending SMSs, you must execute setup() function");
		return -1;
	}

	switch (_send_state) {
	case 0:
		_gsm_serial->print("AT+CMGS=\"");
		_gsm_serial->print(numberSms);
		_gsm_serial->print("\"\r\n");
		_send_state++;
		break;
	case 1:
		read_serial(tmpStateCmgs);
		if (_buffer.indexOf(tmpStateCmgs) != -1) {
			_buffer = "";
			_send_state++;
		}
		break;
	case 2:
		// print a message
		_gsm_serial->print(dataSms); //Text of the SMS
		_gsm_serial->print(static_cast<char>(0x1A)); // 
		_gsm_serial->flush();  //Wait for buffer to be fully wirtten
		_send_state++;
		break;
	case 3:
		read_serial();
		if (_buffer.indexOf("OK") != -1) {
			_buffer = "";
			_send_state++;
			return 1;
		}
	default:
		_send_state = 0;
		_smsToSend = 0;
		break;
	}

	return 1;
}

/**
 * @brief Function called only by the USER
 * it schedule the send of an SMS
 * 
 * @param number Number to send the SMS to
 * @param text 	 Data of the SMS to send
 * @return int 	 1 on sucess, 0 on fail
 */
int SIM800L_SMS::send(String& number, String& text)
{
	if (_smsToSend == 1)
		return -1;
	
	numberSms 	= number;
	dataSms 	= text;
	_smsToSend = 1;
	return 1;
}

/**
 * @brief This function only call the other function send().
 * Since SMS are programmed to be sent (TO NOT BE BLOCKING), this function calls 
 * send to hanle programmed SMS. 
 * 
 * This function MUST be called periodically.
 * 
 * Last time function added. To correct
 * 
 * @return int 
 */
int SIM800L_SMS::loop()
{
	send();

	return 0;
}

/**
 * @brief Send command to module to delete all SMS, Sent and recived
 * 
 * @return int returns the number of deleted SMS
 *  -1 when error
 */
int SIM800L_SMS::delete_all_SMS()
{
	if (_deleteSms_state == 0) {
		_gsm_serial->print("AT+CMGDA=\"DEL ALL\"\r\n");

		_deleteSms_state++;
	} else if (_deleteSms_state == 1) {
		read_serial();

		if (_buffer.endsWith("OK\r\n") == 0) { //If false. Answer not OK
			_buffer = "";
			_deleteSms_state = 0; // Reset state machine

			int deleted_sms = _sms_count;
			_sms_count = 0;

			return deleted_sms;
		} else {
			_timeout_count++;
			if (_timeout_count > TIMEOUT_COUNT_LIMIT) {
				_timeout_count = 0;
				_deleteSms_state--;
			}
		}
	}

	return 0;
}

/**
 * @brief Return the value of _sim800l_initialized.
 * It indicates if the module has been detected or not
 */
int SIM800L_SMS::initialized()
{
	return _sim800l_initialized;
}

/*Global object. Only one instance of SIM800L_SMS in this case*/
SIM800L_SMS SMS;