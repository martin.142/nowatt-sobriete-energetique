/**
 * @file sim800l_sms.h
 * @author Martin Acosta (martin.acostaec@gmail.com)
 * @brief Header for linky_protocol library.
 * Comments and description for each function are placed in the .cpp file.
 * 
 * Library based on the following respositories:
 * 	- https://github.com/vittorioexp/Sim800L-Arduino-Library-revised
 *  - https://github.com/vshymanskyy/TinyGSM
 * 
 * This library is intended to only exploit only the SMS functions of the SIM800L
 * and no other function.
 * The other main difference is that operations are NON BLOCKING, this to no block
 * the processor while waiting for an answer. This is made by using state machines for each operation.
 * Meaning there is a variable state for each operation, the only constraint is that
 * the function MUST be called periodically.
 * 
 *  
 * 
 * @version 0.1
 * @date 2022-07-01
 *
 * @copyright Copyright (c) 2022
 *
 */

// cp -r sim800l_sms/ /home/martin/snap/arduino/70/Arduino/libraries/


#ifndef __SIM800L_SMS_H__
#define __SIM800L_SMS_H__

#include <HardwareSerial.h>

#define MAX_SMS_SIZE 160
#define SERIAL_BUFFER_SIZE 256
#define TIMEOUT_COUNT_LIMIT 3

class SIM800L_SMS
{
	HardwareSerial *_gsm_serial;

	int _sim800l_initialized;
	int _sms_count;
	String _buffer;
	int _timeout_count;

	/*Variables to handle state machines*/
	int _setup_state;
	int _deleteSms_state;
	int _send_state;
	int _read_state;

	String numberSms;
	String dataSms;
	int _smsToSend;

	int 	read_serial		(const char *wairFor);
	int 	print_serial	(char *buf1, String buf2, String buf3, String buf4);
	void 	flush_serial	();
	int 	send			();

public:
	 SIM800L_SMS();
	~SIM800L_SMS();

	int setup			(HardwareSerial *newSerial, int baud, int rx_pin, int tx_pin);
	int read			(String& sender, String& buf);
	int send			(String& number, String& text);
	int loop			();
	int available		();
	int delete_all_SMS	();
	int initialized		();
};

extern SIM800L_SMS SMS;

#endif