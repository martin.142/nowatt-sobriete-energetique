/**
 * @file debug_at.ino
 * @author Martin Eduardo ACOSTA CORRAL (martin.acostaec@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <HardwareSerial.h>

HardwareSerial serialGSM(1);

String message;
String messageGsm;

void serial_event_gsm()
{
	char byte;
	while (serialGSM.available() > 0) {
		messageGsm += (char)serialGSM.read();
	}
}

void print_gsm_serial()
{
	Serial.print("HWSerial - ");
	Serial.print(messageGsm);
}

void serial_terminal()
{	
	while (Serial.available() > 0) {
		message += (char)Serial.read();
	}
}

void setup()
{
	Serial.begin(9600);
	serialGSM.begin(9600, SERIAL_8N1, 12, 13);

	Serial.println("Serial port Initialized");
	Serial.flush();

	
}

void loop()
{
	if (serialGSM.available()) {
		serial_event_gsm();
		if (messageGsm.indexOf('\n') != -1) {
			print_gsm_serial();
			messageGsm = "";
		}
	}

	if (Serial.available()) {
		serial_terminal();
		if (message.indexOf('\n') != -1){ //Check for the end of the message
			serialGSM.print(message);
			message = "";
		}
	}
}