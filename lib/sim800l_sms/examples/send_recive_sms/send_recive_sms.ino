/**
 * @file send_recive_sms.ino
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-07-08
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <HardwareSerial.h>
#include "sim800l_sms.h"

#define TX_PIN 12
#define RX_PIN 13

String number = "+33749187590";

HardwareSerial serialGsm(1);

void cmd_serial()
{
	String data;
	while(Serial.available() > 0) {
		data += (char)Serial.read();
	}

	if (data.indexOf("SEND_SMS=-") != -1) {
		String smsData = data.substring(data.indexOf("=-") + 2);
		Serial.print("Sending SMS to : ");
		Serial.print(number);
		Serial.print(" : ");
		Serial.println(smsData);

		SMS.send(number, smsData);
	} else if (data.indexOf("SEND_SMS_TO=+") != -1 and data.indexOf("=-") != -1) {
		String smsNumber = data.substring(data.indexOf("=+") + 1 , data.indexOf("=-"));
		String smsData   = data.substring(data.indexOf("=-") + 2);
		Serial.print ("Sending SMS to: ");
		Serial.print(smsNumber);
		Serial.print(" Data: ");
		Serial.println(smsData);

		SMS.send(smsNumber, smsData);
	} else if (data.indexOf("DELETE_ALL_SMS") != -1) {
		Serial.println("Deleting all messages");
		int nbSmsDel = SMS.delete_all_SMS();
		if (nbSmsDel == -1)
			Serial.println("ERROR Deleting sms");
		Serial.print("SMS deleted: ");
		Serial.println(nbSmsDel);
	} else {
		Serial.print("INCORRECT INPUT\n");
	}
}

void setup()
{
	Serial.begin(9600);
	Serial.println("Starting Programm");
	Serial.flush();
	cmd_serial();
	delay(500);

	Serial.println("Initializzing SMS Serial Communication");

	if (SMS.setup(&serialGsm, 9600, RX_PIN, TX_PIN)) {
		Serial.println("Error, setup didn't go well");
	}
}

int sendOnce = 0;

void loop()
{
	String sender; 
	String data;

	if( SMS.read(sender, data) != -1) {
		Serial.print("SMS recived from: ");
		Serial.print(sender);
		Serial.print(" : ");
		Serial.println(data);
	}

	String num("+33749187590");
	String dat("Test message using ESP32 and SIM800L");

	/* if (!sendOnce)
		SMS.send(num, dat); */

	if (Serial.available())
		cmd_serial();

	delay(1000);
}

