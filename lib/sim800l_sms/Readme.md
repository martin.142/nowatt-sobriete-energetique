# Library - SIM800L SMS

This is a basic library developed for the project **noWatt** to use the gsm module SIM800L to send and recive messages in a non blocking way

# Library Description

This library implements the basic functions to interact with the gsm module SIM800L to send and recive SMS. It uses UART to comunicate between the ESP32 and the SIM800L module. The developement board that I am using to develop this project (ESP32 TTGO LoRa) avoid the usage of a native UART port, so I can not use the hardware interrupt when reciving data from the UART Port. For this reason this libary has  been developped to be non blocking function that must be called periodically in order to read data and detect incoming information from the module.