
# Library - Linky protocol

This is a basic library developed for the project **noWatt** to read the data out of the Linky Counter

# Library Description

The LINKY counter has a TIC (Télé-information Client) port that provides information, within them, instantaneus energy consomption. This data is provided via a serial communication, there is some electronics to implement to make the signal readable by the UART port of the ESP32, the diagram is just below. 

DIAGRAM