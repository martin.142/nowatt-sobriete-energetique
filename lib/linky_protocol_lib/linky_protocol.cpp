/**
 * @file linky_protocol.cpp
 * @author Martin Acosta (martin.acostaec@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-07-12
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "linky_protocol.h"
#include <string.h>

/**
 * @brief Construct a new linky protocol::linky protocol object
 * 
 */
LINKY_PROTOCOL::LINKY_PROTOCOL() 
{
	_val_current 	= -1;
	_val_power 		= -1;
	_val_voltage 	= -1;
	_state 			= 0;
	_dataAvailable	= 0;
	_tmp_nbBytes	= 0;
}

/**
 * @brief Destroy the linky protocol::linky protocol object
 * 
 */
LINKY_PROTOCOL::~LINKY_PROTOCOL() {}


/**
 * @brief Only for DEBUG and ERROR Printing
 * 
 */
int LINKY_PROTOCOL::print_serial(char *buf1, String buf2 = "", String buf3 = "", String buf4 = "")
{
	Serial.print("[LINKY] - ");
	Serial.print(buf1);
	Serial.print(buf2);
	Serial.print(buf3);
	Serial.println(buf4);

	return 0;
}

/**
 * @brief flush rx buffer from serial port
 */
void LINKY_PROTOCOL::flush_rx_serial()
{
	char temp;

	while (_serialPort->available() > 0)
		temp = _serialPort->read();

	_state = 0;
}

/**
 * @brief Gets data from lables IINST and PPAP
 * which stands for Instataneus current and 
 * Aparent Power
 * 
 */
void LINKY_PROTOCOL::getNumericIAP()
{
	int v, i, p;
	String strV, strI, strP;

	int indexIINST = _allData.indexOf("IINST");
	if (indexIINST == -1)
		return;

	strI = _allData.substring(indexIINST+6, indexIINST+9);
	_val_current = strI.toInt();

	int indexPPAP = _allData.indexOf("PPAP");
	if (indexPPAP == -1)
		return;

	strP = _allData.substring(indexPPAP+5, indexPPAP+10);
	_val_power = strP.toInt();

	if (_val_current <= 0)
		return;

	_val_voltage = _val_power / _val_current;
}

/**
 * @brief Initializes Serial port
 * 
 * @param newSerial Pointer of a HardwareSerial object
 * @param baud 		Baud speed
 * @param rx_pin 	Rx gpio
 * @param tx_pin 	Tx gpio
 * @return int 		0 on succes, -1 on error
 */
int LINKY_PROTOCOL::setup(HardwareSerial *newSerial, int baud, int rx_pin, int tx_pin)
{
	if (!newSerial)
		return -1;

	if (_initialized)
		return -1;

	_serialPort = newSerial;

	_serialPort->begin(baud, SERIAL_8N1, rx_pin, tx_pin);
	if (!_serialPort) {
		_serialPort = NULL;
		print_serial("Setting up FAILED");
		return -1;
	}

	_initialized = 1;
	return 0;
}

/**
 * @brief FUNCTION MUST BE CALLED PERIODICALLY 
 * 
 * Read frames sent by the linky TIC port. It updates the
 * variable _allData with the firts frame readed. The end and the
 * begining of the frame is marked with the characters STX (0x02) et 
 * ETX (0x03), the variable _state works as a state machine to identify
 * the STX when 0, then _state == 1 add every charcater to the _AllDataTmp
 * then when the character ETX arrives, it affects the _allData string
 * whit the full frame already stored at _allDataTmp.
 * 
 * This function is not blocking and it SHOULD be called 
 * periodically with a low period. If there is a long period of time
 * between two calls, it is recomender to call flush_rx_serial() to ignore
 * oldData.
 * 
 * This function does not return the frameString to the user application,
 * to get de data, the function getAllData()
 * 
 */
void LINKY_PROTOCOL::readData()
{
	int tmp;
	int nbBytes;

	if (_state == 0) { //Waits for STX character
		while (_serialPort->available()) {
			tmp = _serialPort->read();
			if (tmp == STX) {
				_state = 1;
				_allDataTmp = "";
				_tmp_nbBytes = 0;
				break;
			}
		}
	} 
	
	if(_state == 1) { //Saves characters until ETX character
		while (_serialPort->available()) {
			tmp = _serialPort->read();
			_tmp_nbBytes++;
			if (tmp == ETX) {
				_state = 0;
				_allData = _allDataTmp;
				_allDataTmp = "";
				_dataAvailable = 1;
				return;
			}
			if (_tmp_nbBytes > 190) { //Data Lost
				_state = 0;
				return;
			}
			_allDataTmp += (char)tmp;
		}
	}
}


/**
 * @brief Return the strnig stored in _allData
 * _allData stores the last full frame identified by
 * the function readData()
 * 
 * @return String Last full TIC frame
 */
String LINKY_PROTOCOL::getAllData()
{
	getNumericIAP();
	_dataAvailable = 0;
	return _allData;
}

/**
 * @brief Return power value.
 * 
 * @return int power value, if -1, variable not updated
 * To update, call getNumericIAP()
 */
int LINKY_PROTOCOL::getPower()
{
	return _val_power;
}

/**
 * @brief Return current value.
 * 
 * @return int current value, if -1, variable not updated
 * To update, call getNumericIAP()
 */
int LINKY_PROTOCOL::getCurrent()
{
	return _val_current;
}

/**
 * @brief Return voltage value.
 * 
 * @return int voltage value, if -1, variable not updated
 * To update, call getNumericIAP()
 */
int LINKY_PROTOCOL::getVoltage()
{
	return _val_voltage;
}

String LINKY_PROTOCOL::getMainInfo()
{
	return "";
}

int LINKY_PROTOCOL::setMainInfo(String &dataIn)
{
	return 0;
}

/**
 * @brief Retrurn the value of _dataAvailable.
 * indicates if new data readen is available so the user
 * can call getAllData and derivates
 * 
 * @return int 1 if new data available
 * 0 if no data available, if multiple calls to this function returns
 * 0, then no data is being readed by the UART Port
 */
int LINKY_PROTOCOL::dataAvailable()
{
	return _dataAvailable;
}

/**
 * @brief Return 1 if class / Serial communication initialized, 0 if not
 */
int LINKY_PROTOCOL::isInitialized()
{
	return _initialized;
}


LINKY_PROTOCOL LINKY;
