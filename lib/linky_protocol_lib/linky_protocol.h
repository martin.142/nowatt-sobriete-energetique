/**
 * @file linky_protocol.h
 * @author Martin Acosta (martin.acostaec@gmail.com)
 * @brief Header for linky_protocol library.
 * Comments and description for each function are placed in the .cpp file.
 * 
 * This library includes the function to read and interprete the linky protocol data
 * recived from the serial port of the linky counter. There are some electronic
 * betweeen the esp32 and the serial port of the linky port to install so the signal
 * is readable by the UART port of the esp32.
 * 
 * Linky has two modes of function (Old version of linky has only the fisrt one)
 *  - Historic mode : Works at badu 
 *  - Standard mode : Works at 9600 badus
 * 
 * MODIFICATIONS NEEDED
 * Add mode selection when calling the constructor
 * Add mode Sandard to the library repertory
 * PROBABLY add statistics calulation here for each value readed
 * Create a structure to store each value in its corresponding data type
 * 		So the application does not need to cast every value from raw datad
 * 
 * 
 * 
 * @version 0.1
 * @date 2022-07-12
 * 
 * @copyright Copyright (c) 2022
 * 
 */

//cp -r linky_protocol_lib/ /home/martin/snap/arduino/70/Arduino/libraries/

#ifndef __LINKY_PROTOCOL_H__
#define __LINKY_PROTOCOL_H__

#include <HardwareSerial.h>

#define STX 0x02	//Start Character from TIC frame
#define ETX 0x03	//End character from TIC frame

class LINKY_PROTOCOL {

	HardwareSerial *_serialPort;
	String _allData;		//Last readed Raw TIC data
	String _allDataTmp;		//Raw TIC data while reading
	String _mainData;		//Filtred data
	String _mainDataConfig; //Labels of data to be classed as main data
	int _initialized;
	int _tmp_nbBytes;

	int _val_power;
	int _val_voltage;
	int _val_current;

	int _state;				//State machine for lecture
	int _dataAvailable;		//Indicate wheter a new data in _allData is available

	void 	flush_rx_serial	();
	void 	getNumericIAP	();
	int 	print_serial	(char *buf1, String buf2, String buf3, String buf4);

public:

	 LINKY_PROTOCOL();
	~LINKY_PROTOCOL();
	
	int 	setup		(HardwareSerial *newSerial, int baud, int rx_pin, int tx_pin);
	void 	readData	();

	String 	getAllData		();
	String 	getLabelValue	();
	String 	getMainInfo		();
	int 	getPower		();
	int 	getCurrent		();
	int 	getVoltage		();
	int 	setMainInfo		(String &dataIn);
	int 	dataAvailable	();
	int 	isInitialized	();
};

extern LINKY_PROTOCOL LINKY;

#endif
