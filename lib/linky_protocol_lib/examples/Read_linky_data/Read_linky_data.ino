/**
 * @file Read_linky_data.ino
 * @author Martin Eduardo ACOSTA CORRAL (martin.acostaec@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-07-13
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <HardwareSerial.h>
#include "linky_protocol.h"

#define TX_PIN 12
#define RX_PIN 13
#define MAX_WAIT_FOR_TIMER 2

HardwareSerial serialLinky(2);

unsigned int waitFor(int timer, unsigned long period){
  static unsigned long waitForTimer[MAX_WAIT_FOR_TIMER];  // il y a autant de timers que de tâches périodiques
  unsigned long newTime = micros() / period;              // numéro de la période modulo 2^32 
  int delta = newTime - waitForTimer[timer];              // delta entre la période courante et celle enregistrée
  if ( delta < 0 ) delta = 1 + newTime;                   // en cas de dépassement du nombre de périodes possibles sur 2^32 
  if ( delta ) waitForTimer[timer] = newTime;             // enregistrement du nouveau numéro de période
  return delta;5000000
}

int stateSimulateTic = 0;

void write_TIC_label(char *label, char *info)
{
		serialLinky.print(static_cast<char>(0x0A));
		serialLinky.print(label);
		serialLinky.print(static_cast<char>(0x09));
		serialLinky.print(info);
		serialLinky.print(static_cast<char>(0x09));
		serialLinky.print('X');
		serialLinky.print(static_cast<char>(0x0D));
		serialLinky.flush();
}

void simulate_TIC()
{
	if (!(waitFor(13, 16700)))
		return;

	int dataI = random(0, 30);

	String ppap(dataI * 220, DEC);
	int len = ppap.length();
	if (len == 1) {
		ppap = "0000" + ppap;
	} else if (len == 2) {
		ppap = "000" + ppap;
	} else if (len == 3) {
		ppap = "00" + ppap;
	} else if (len == 4) {
		ppap = "0" + ppap;
	}

	switch (stateSimulateTic)
	{
	case 0:
		linkySerialGlobal.print(static_cast<char>(STX));
		write_TIC_label("ADCO", "022061021363");
		stateSimulateTic++;
	case 1:
		write_TIC_label("OPTARIF", "HC..");
		stateSimulateTic++;
	case 2:
		write_TIC_label("ISOUSC", "45");
		stateSimulateTic++;
	case 3:
		write_TIC_label("HCHC", "005390280");
		stateSimulateTic++;
	case 4:
		write_TIC_label("HCHP", "007914071");
		stateSimulateTic++;
	case 5:
		write_TIC_label("PTEC", "HP..");
		stateSimulateTic++;
	case 6:
		write_TIC_label("IINST", (char*)String(dataI, DEC).c_str());
		stateSimulateTic++;
	case 7:
		write_TIC_label("IMAX", "090");
		stateSimulateTic++;
	case 8:
		write_TIC_label("PPAP", (char*)ppap.c_str()); //"03440"
		stateSimulateTic++;
	case 9:
		write_TIC_label("HHPHC", "A");
		stateSimulateTic++;
	case 10:
		write_TIC_label("MOTDETAT", "000000");
		linkySerialGlobal.print(static_cast<char>(ETX));
		linkySerialGlobal.flush();
		stateSimulateTic++;
	case 11:
		stateSimulateTic++;
		break;
	case 12:
		stateSimulateTic = 0;
		break;
	default:
		stateSimulateTic = 0;
		break;
	}

	return;
}

void setup()
{
	Serial.begin(9600);
	delay(500);
	Serial.println("Send anything via serial to start programm");
	Serial.flush();
	while(!Serial.available());

	Serial.println("Starting programm");

	if (LINKY.setup(&serialLinky, 9600, RX_PIN, TX_PIN)) {
		Serial.println("Error initializing LINKY OBJECT");
	}

	Serial.print("initial I val: "); Serial.println(LINKY.getCurrent());
	Serial.print("initial S val: "); Serial.println(LINKY.getPower());
	Serial.print("initial V val: "); Serial.println(LINKY.getVoltage());

	stateSimulateTic = 0;
}

void loop()
{
	if (LINKY.isInitialized() == 0) {
		LINKY.setup(ctx->linky_serial, 9600, RX_PIN, TX_PIN);
		return;
	}

	simulate_TIC();

	LINKY.readData();

	if ((waitFor(1, 5000000))) {
		if (LINKY.dataAvailable()) {
			Serial.println("Reading data from linky");
			Serial.println(LINKY.getAllData());
		}
	}
}