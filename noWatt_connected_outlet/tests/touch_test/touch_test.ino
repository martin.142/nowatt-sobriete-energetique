/**
 * @file touch_test.ino
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-08-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/**
 * @brief This programm is bassed on the folowing example
 * https://randomnerdtutorials.com/esp32-touch-pins-arduino-ide/
 */

/**
 * @brief Available GPIOS for touch on the esp32 ttgo lora board
 * 13, 14, 27
 */

#define TOUCH_PIN_0 27
#define SHORT_TOUCH_MIN_MS 25
#define SHORT_TOUCH_MAX_MS 1000

unsigned long previousTime = 0;
int shortP = 0;
int longP = 0;

void short_press()
{
	Serial.println("Short Press");
}

void long_press()
{
	Serial.println("Long Press");
}

int touchPressType()
{	
	unsigned long current_time = millis();
	unsigned long elapsed_time = current_time - previousTime;
	if (get_touch_boolean()) {
		if (elapsed_time < SHORT_TOUCH_MAX_MS and elapsed_time > SHORT_TOUCH_MIN_MS) {
			if (!shortP) {
				//short_press();
				shortP = 1;
			}
			return 1;
		} else if (elapsed_time >= SHORT_TOUCH_MAX_MS) {
			if (!longP) {
				long_press();
				longP = 1;
			}
			return 2;
		} 
	} else {
		previousTime = current_time;
		if (shortP == 1 and longP == 0) {
			short_press();
		}
		shortP = 0;
		longP = 0;
	}

	return 0;
}

int get_touch_boolean()
{
	return (touchRead(TOUCH_PIN_0) < 35) ? 1 : 0;
}

int get_touch_value()
{
	touchRead(TOUCH_PIN_0);
}

void setup()
{
	Serial.begin(9600);
	delay(1000); // give me time to bring up serial monitor
	Serial.println("ESP32 Touch Test");
}

void loop()
{
	touchPressType();
}
