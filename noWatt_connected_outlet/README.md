# noWatt base station

Secondary station of the noWatt project. Connected electric outlet.

# Project Description

This sub-project allows to have a controlable electric outlet. The main objective is to be able to disconnect some component that, in its IDLE states usually consome some energy, to disconnect it from the prower supply. This action reduces the energy consomption. For the same reason, the component must be energy saver, so there will be some technologies used in the main programm intended to offer the best fonctionnalities and power saving modes. 

# Hardware

 - 1 ESP32 TTGO LORA
 - 1 Realy module x4
 - 1 Male Europena 220v electric outlet
 - 1 Female European 220v electric outlet

 ![Main diagram](./diagrams/img/connectedOutletDiagram.png "Connected Outlet schematic")

# How to install the project

Dependencies: 
    Libraries

# Functionalities

 - **WIFI** : communicates with the base station via WIFI to operate the conmutator.
 - **BLE** : Communicaties with the base station via BLE to operate the conmutator.
 - **LORA** : Communicates with the base station via LORA to operate the conmutator.
 - **BUTTON** : The integratet button allows to operate the conmutator manually, by pressing the button, it complements the current state.
 - **TOCUH** : The esp32 integrate some TOUCH pins, one of them has been configured to do the same action as the button. Complement the current.value of the contact when a tocuh is recived.


