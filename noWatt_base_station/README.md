# noWatt base station

Main station of the noWatt project.

# Project Description

This project consists in developing a base station to read the information out of the linky counter. This information is obtanied from the serial UART serial port of the linky counter using the microcontroller ESP32. The base station includes also multiple functionalities that allows to communicate with other components in order to create a complete domotic system.

# Hardware

 - 1 - ESP32 TTGO LORA
 - 1 - SIM800L V1
 - 1 - LM1086 CT-ADJ
 - 1 - Resistor 150 ohm
 - 1 - Resistor 390 ohm
 - 1 - Resistor ??? ohm
 - 1 - Photo-resistor ??? ohm
 - 1 - Push button

![Main diagram](./diagrams/img/mainDiagram.png "Base station schematic")

# How to install the project

Dependencies: 
    Libraries

# Functionalities

- **Linky reader** : The communication with the linky counter is relatively easy, it is a simple serial communications. The functionalities implemented into the base station is to detect the TIC frames and get useful data to calculate some statistics. The objective is to set some limits so it can alarm the user when there is a anrmal consomation or to notify the mean daily consomation so that the user can easly adjust their energy consomation habtis.
- **Display** : Multiple screens availables, the button allows to change screen with a short press, and long press executes the options for each screen.
- **GSM Module** : Send recived SMS. The sistems will send alarms if a threshold for consoption were set. It also wil recive SMS to execute some commands and, in some cases, a response will be revived, in others, an action by the system will be exeuted.
- **BLE communcation** : (IN DEVELOPEMENT) this module scans periodically to detect BLE components that could send data through a service or just as a BEACON device.
- **MQTT clinet** : (IN DEVELOPEMENT) All data will be available throught the mqtt server if there is a connection. Commands can be recived from mqtt too.

## Commands

Command can be call from SMS, Serial, MQTT et BLE (MQTT et BLE not avalailable yet). If there is an response, the sender will directly recive the response. There are command that will be exculsive for a given source and all commands are text commands.

The syntax of a commands is : ```"CMD"<space><COMMAND><space>"-v"<value1><space>"-v"<space><value2><space>"-v"<space><value3> ...```

 - **Available commands:**
 - CMD DEEP SLEEP
 - CMD LIGHT SLEEP
 - CMD SEND SMS TO -v \<destNum> -v \<smsData>
 - CMD SEND SMS -v \<smsData>
 - CMD DELETE ALL SMS
 - CMD STATUS
 - CMD LIGHT LEVEL
 - CMD BOOT COUNT
 - CMD LINKY MAIN DATA}
 - CMD LINKY STATS DATA
 - CMD GET TIME

## Screens
