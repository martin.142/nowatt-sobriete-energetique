/**
 * @file gsm_test.ino
 * @author Martin Eduardo ACOSTA CORRAL (martin.acostaec@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <HardwareSerial.h>

HardwareSerial serialGSM(1);

char message[100];

void serial_event_gsm()
{
	char temp[100];
	int count = 0;
	while(serialGSM.available() > 0) {
		temp[count] = serialGSM.read();
		count++;
	}
}

void serial_terminal()
{
	int count = 0;
	memset(message, 0, 100);

	while (Serial.available() > 0) {
		message[count] = Serial.read();
		count++;
	}
	message[count] = 0;
}

Light_Class class1(1, 500, 3);

void setup()
{
	Serial.begin(9600);
	serialGSM.begin(9600, SERIAL_8N1, 12, 13);

	Serial.println("Serial port Initialized");
}

void loop()
{
	if (serialGSM.available()) {
		serial_event_gsm();
	}

	if (Serial.available()) {
		serial_terminal();
		serialGSM.println(message);
	}
}

