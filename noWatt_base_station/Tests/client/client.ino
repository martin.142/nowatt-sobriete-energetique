#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Arduino.h>
#include <WiFi.h>
#include <PubSubClient.h>

/*DEFINE for Display*/
#define SCREEN_WIDTH 128    // OLED display width, in pixels
#define SCREEN_HEIGHT 64    // OLED display height, in pixels
#define OLED_RESET     16   // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C // See datasheet for Address; 0x3C for 128x64, 0x3D for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

/*Used pins*/
#define BUTTON_PIN  23
#define BUZZER_PIN  17
#define PIN_SDA     4
#define PIN_SCL     15

/*Number of timers availables*/
#define MAX_WAIT_FOR_TIMER 10

/*Analog port configuration*/
#define ANALOG_MAXVAL     4096
#define PWM_FREQ_BASE          5000
#define PWM_LEDCHANNEL    0
#define PWM_BUZZERCHANNEL 1
#define PWM_RESOLUTION    8   //0 - 255


/* WIFI and MQTT Global Declaratins*/
const char* ssid = "Martin";
const char* password = "martinacosta";

const char* mqtt_server = "192.168.43.193";
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int  value = 0;

/**
 * waitFor is a non blocking function that returns the number periods
 * passed science the last function call for the specified timer.
 */
int waitFor(int timer, unsigned long period){
  static unsigned long waitForTimer[MAX_WAIT_FOR_TIMER];  // il y a autant de timers que de tâches périodiques
  unsigned long newTime = micros() / period;              // numéro de la période modulo 2^32 
  int delta = newTime - waitForTimer[timer];              // delta entre la période courante et celle enregistrée
  if ( delta < 0 ) delta = 1 + newTime;                   // en cas de dépassement du nombre de périodes possibles sur 2^32 
  if ( delta ) waitForTimer[timer] = newTime;             // enregistrement du nouveau numéro de période
  return delta;
}

/**
 * Déclaration structure des tâches au debut à fin d'éviter l'erreur INVALID USE OF INCOMPLET TYPE
 */
struct Boite_s{
  int flag;
  unsigned int bData;
};

struct Led_s {
  int timer;              // numéro du timer pour cette tâche utilisé par WaitFor
  unsigned long period;   // periode de clignotement
  int pin;                // numéro de la broche sur laquelle est la LED
  int etat;               // etat interne de la led
  int duty;
}; 

struct Mess_s {
  int timer;              // numéro de timer utilisé par WaitFor
  unsigned long period;   // periode d'affichage
  char mess[20];
} ; 

struct Ligth_s{
  int timer;            
  int period;
  int pin;
  unsigned int val;
};

struct Display_s{
  int timer;
  int period;
  String stringData;    //Data a afficher sur plusieures lignes. 21 characters par lige
};

struct Button_s{
  int timer;
  int period;
  int pin;
  int value;
};

struct Buzzer_s{
  int timer;
  int period;     //Periode entre appels a la fonction
  int pin;
  int value;
  int freq;       //Frenquece du signal en sortie
};

/**
 * ----------- Déclaration des tâches
 */
struct Led_s Led1;
struct Mess_s Mess1;
struct Display_s Disp1;
struct Ligth_s Light1;
struct Button_s Button1;
struct Buzzer_s Buzzer1;

/**
 * ----------- Déclaration des boites aux lettrs (moyen de communication inter-tâche)
 */
struct Boite_s Boite1;
struct Boite_s BoiteLed;
struct Boite_s BoiteBtn_Bzz;

/**
 * --------- Definition of setup and loop tache
 */

void setup_Led( struct Led_s * ctx, int timer, unsigned long period, byte pin) {
  ctx->timer = timer;
  ctx->period = period;
  ctx->pin = pin;
  ctx->etat = 1;
  ctx->duty = 0;

  /**
   * Configuration du canal PWM pour la led. 
   */
  ledcAttachPin(pin, PWM_LEDCHANNEL);
  ledcSetup(PWM_LEDCHANNEL, PWM_FREQ_BASE, PWM_RESOLUTION);
}

void loop_Led( struct Led_s * ctx, struct Boite_s *boiteLed) {
  if(!(waitFor(ctx->timer,ctx->period))) return;

  float tmpDutyCycle; 
  
  if(boiteLed->flag ==  1){ //Si flag = 1, Message pendants
    boiteLed->flag = 0;
    ctx->duty = boiteLed->bData;  //Lécture et stockage du message

    /**
     * Tester que duty cycle != de zero sino divition par zero. Exeption
     * Calculer le rapport cyquile de 0 - 1 avec point décimal
     */
    if(ctx->duty != 0)tmpDutyCycle = (float(ctx->duty) / (float)100);
    else tmpDutyCycle;

    /**
     * 0 -> 0% ..... 128 -> 50% ....... 255 -> 100%
     */
    ledcWrite(PWM_LEDCHANNEL, 255*tmpDutyCycle); 
  }                
}

//--------- definition de la tache Mess

/**
 * Message affiches des information sur la ligne de commande
 */
void setup_Mess( struct Mess_s * ctx, int timer, unsigned long period, const char * mess) {
  ctx->timer = timer;
  ctx->period = period;
  
  strcpy(ctx->mess, mess);
  Serial.begin(9600);     // initialisation du débit de la liaison série
}

void loop_Mess(struct Mess_s *ctx, struct Led_s *led, struct Boite_s *boite) {
  if (!(waitFor(ctx->timer,ctx->period))) return;         // sort s'il y a moins d'une période écoulée
  
  Serial.print(ctx->mess); // affichage du message
  Serial.print(" PWM Level: ");
  Serial.print(led->duty);
  Serial.print(" Light leve: ");
  Serial.println(boite->bData);
}


//----------- Definition de la tache light sensor 

void setup_Light(struct Ligth_s *ctx, int timer, int pin, int period){
  ctx->timer = timer;
  ctx->period = period;
  ctx->pin = pin;
}

void loop_Light(struct Ligth_s *ctx, struct Boite_s *boite, struct Boite_s *boiteLed){
  /**
   * On envoi des valuer par boite aux lettre. On ne teste pas si la valuer précédente a été lue, 
   * On n'as pas besoir d'aquiter la reception d'une donné
   */
  if ((waitFor(ctx->timer,ctx->period))){ 
    ctx->val = analogRead(ctx->pin);

    //Convertir la valeur de 0-4096 à 0% - 100%
    unsigned int valTmp = (unsigned int)(((float)(ANALOG_MAXVAL - ctx->val) / (float)ANALOG_MAXVAL) * 100);
    boite->bData = valTmp;
  }
  
  if(boiteLed->flag == 0){  //Si != de 0. Les valeurs précedences n'ont pas encore été traités
    if ((waitFor(ctx->timer+1,ctx->period*5))){ 
      boiteLed->flag = 1; //Le flag indique à la tâche lecteur qu'il y a un donnée READY

      unsigned int valTmp = (unsigned int)(((float)(ANALOG_MAXVAL - ctx->val) / (float)ANALOG_MAXVAL) * 100);
      boiteLed->bData = valTmp;
      if(boiteLed->bData == 0) boiteLed->bData = 1; //Eviter que PWM égal a 0
    }
  }
}


// ----------- definition de la tache display

void setup_Display(struct Display_s * ctx, int timer, int period){
  ctx->timer = timer;
  ctx->period = period;
  ctx->stringData = "String Test";
  
  Wire.begin(PIN_SDA, PIN_SCL); // Initialisation communication I2C
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
  }
  display.display();
  delay(2000); // Pause for 2 seconds
  display.clearDisplay();
}

void loop_Display(struct Display_s *ctx, struct Led_s *led, struct Ligth_s *light, struct Button_s *button, struct Buzzer_s *buzzer){
  if (!(waitFor(ctx->timer,ctx->period))) return;   // return si la periode n'est pas encore passé

  /**
   * Transforme la valeur analogic en pourcentage 0 - 100%
   */
  unsigned int rawToPrc = (unsigned int)(((float)(ANALOG_MAXVAL - light->val) / (float)ANALOG_MAXVAL) * 100);

  /**
   * Construction du string à affciher.
   * Tout le string est stocké dans l'attribut du Display.
   * 
   * la fonction appendLine permet de remplir une linge avec de SPACE pour 
   * avoir l'inforrmation suivant dans la ligne suivante.
   */

  //Light LEVEL
  ctx->stringData = "Light Level: ";
  ctx->stringData += String(rawToPrc, DEC);
  ctx->stringData += "%";
  
  appendLine(ctx->stringData);

  //PWM LEVEL
  ctx->stringData += "PWM Level: ";
  ctx->stringData += led->duty;
  ctx->stringData += "%";

  appendLine(ctx->stringData);

  //BTN VALUE
  ctx->stringData += "Btn Value: ";
  ctx->stringData += button->value;

  appendLine(ctx->stringData);

  //BUZZER FREQUENCE
  ctx->stringData += "Buzzer Freq: ";
  ctx->stringData += buzzer->freq;
  ctx->stringData += "Hz";

  appendLine(ctx->stringData);

  //BUZZER STATUS
  ctx->stringData += "Buzzer ";
  ctx->stringData += (buzzer->value == 1)? "Active" : "Not Active";
  
  
  int stringlen = ctx->stringData.length() + 1; //Obtenir la longeur du string
  
  /**
   * Transfomrer le chaine de type string en char*. 
   */
  char tmp[stringlen];                          
  ctx->stringData.toCharArray(tmp, stringlen); // Cast string to char*
  tmp[stringlen] = 0;
  
  testdrawtext(tmp);
}


void testdrawtext(char* text) {
  //21 characters par ligne
  display.clearDisplay();
  display.setTextSize(1);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  
  display.setCursor(0, 0);     // Start at top-left corner
  display.cp437(true);         // Use full 256 char 'Code Page 437' font
  display.write(text);
  display.display();
}

// Concatenate SPACE to pass to next line
void appendLine(String &str){
  for(int i = (str.length() % 21); i < 21 ; i++){
    str += ' ';
  }
}

// -------- Tache button

/**
 * Handler du interruption boutton
 */
void IRAM_ATTR isr() {
  Serial.println("BUTTON INTERRUPTION RECIVED");
}

void setup_Button(struct Button_s *button, int timer, int period, int pin){
  button->timer  = timer;
  button->period = period;
  button->pin    = pin;
  button->value  = 0; //Init value a zero
  
  pinMode(button->pin, INPUT_PULLUP); //l'option INPUT_PULLUP est nécessaire à cause du mode de connection du boutton

  /**
   * Déclaration d'une interruption pour le GPIO boutton. 
   * la fonction appelé sera isr() et FALLING indique le déclanchement
   * de l'interruption au front décendante du signal
   */
  attachInterrupt(button->pin, isr, FALLING); 
}

void publishButtonVal(){
  char tempChar[8];
  String tempString;
  int tempLength;
  
    /*Get Button Value and publish it*/
  tempString = String(Button1.value,DEC);
  tempLength = tempString.length();
  tempLength += 1; /*Take care of null terminated char*/
  tempString.toCharArray(tempChar, tempLength);

  // Publish buttonValue sur topic = esp32/ButtonState
  client.publish("esp32/ButtonState", tempChar);
}

void loop_Button(struct Button_s *button, struct Boite_s *boite){
  if (boite->flag == 1) return; //Valeur pas encore lu
  if (!(waitFor(button->timer,button->period))) return;

  button->value = 1 - digitalRead(button->pin);

  /*Send the message and publish the message when value changed*/
  static int lastVal;
  if(lastVal != button->value){
    boite->flag = 1;
    boite->bData = button->value;
    lastVal = button->value;

    publishButtonVal();
  }
}


//--------- Définition buzzer methodes

void setup_Buzzer(struct Buzzer_s *buzzer, int timer, int period, int pin){
  buzzer->timer   = timer;
  buzzer->period  = period;
  buzzer->pin     = pin;
  buzzer->freq    = 5000;
  buzzer->value   = 0;

  //Configuration du signal PWM  Pour buzzer
  ledcAttachPin(buzzer->pin, PWM_BUZZERCHANNEL);
  ledcSetup(PWM_BUZZERCHANNEL, buzzer->freq, PWM_RESOLUTION);
}

void loop_Buzzer(struct Buzzer_s *buzzer, struct Boite_s *boiteButton){
  /*Static variables, the value is the same at every function call*/
  static int lastVal;
  static int lastFrq;

  /*
   * Check if any message is recived or the time is elapsed
   * 1 -> Message sent from button read function
   * 2 -> Message sent from serial port value recived
   * 3 -> Message sent from MQTT publisher
   */
  if(boiteButton->flag == 0) return; /*No messaged Recived. Return*/
  if(!(waitFor(buzzer->timer,buzzer->period))) return;
  
  buzzer->value = boiteButton->bData;

  /*Detect and execute the command only if the buzzer val or freq has changed*/
  if(lastVal != buzzer->value or lastFrq != buzzer->freq){
    if(buzzer->value){
      ledcWriteTone(PWM_BUZZERCHANNEL, buzzer->freq);
    }else{
      ledcWriteTone(PWM_BUZZERCHANNEL, 0);
    }

    /*Update Values*/
    lastVal = buzzer->value;
    lastFrq = buzzer->freq;
  }

  /*Acknowlege the reseption of the data*/
  boiteButton->flag = 0;
}

void setup_wifi() {
  delay(10);
  
  /*We start by connecting to a WiFi network*/
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  // Create string to shwo in the screen
  String strTmp;
  strTmp += "Connectring to ...";
  appendLine(strTmp);
  
  strTmp += "IP  :";
  strTmp += mqtt_server;
  appendLine(strTmp);
  
  strTmp += "SSID:";
  strTmp += ssid;
  appendLine(strTmp);
  
  int stringlen = strTmp.length() + 1; //Obtenir la longeur du string
  char tmp[stringlen];                          
  strTmp.toCharArray(tmp, stringlen); // Cast string to char*
  tmp[stringlen] = 0;
  
  testdrawtext(tmp);
  
  WiFi.begin(ssid, password);

  char waitTmp[256];
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
    
    strTmp += ".";
    stringlen = strTmp.length() + 1; //Obtenir la longeur du string                       
    strTmp.toCharArray(waitTmp, stringlen); // Cast string to char*
    
    testdrawtext(waitTmp); 
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  strTmp = "WIFI Connected";
  stringlen = strTmp.length() + 1; //Obtenir la longeur du string                       
  strTmp.toCharArray(waitTmp, stringlen); // Cast string to char*
  testdrawtext(waitTmp); 
  
  delay(2000);
}

void reconnect() {
  /*Connection to MQTT broker*/
  
  /*Loop until we're reconnected*/
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      
      /*Subscribe topics. ESP32 will listen to these topics*/
      client.subscribe("esp32/buzzerFreq");
      client.subscribe("esp32/buzzerValue");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup_publisher(){
  
}

void setup_mqtt(const char *server,int port){
  
  client.setServer(server, port);
  client.setCallback(callback);
}

void loop_publish(){
  /*
   * Publisher will publish all the information showed  
   * on the esp32 screen. Light Level, PWM Level, Button state
   * Buzzer Freq et Buzzer State
   */
  
  if(!(waitFor(9,1000000))) return;
  char tempChar[8];
  String tempString;
  int tempLength;
  

  /*Get Light Level and publish*/
  tempString = String(Light1.val,DEC);
  tempLength = tempString.length();
  tempLength += 1; /*add null terminated char*/
  tempString.toCharArray(tempChar, tempLength);
  client.publish("esp32/LightLevel", tempChar);

  /*Get PWM Level and publish*/
  tempString = String(Led1.duty,DEC);
  tempLength = tempString.length();
  tempLength += 1; /*add null terminated char*/
  tempString.toCharArray(tempChar, tempLength);
  client.publish("esp32/PWMLevel", tempChar);

  /*Get Buzzer Freqauency Read and publish*/
  tempString = String(Buzzer1.freq,DEC);
  tempLength = tempString.length();
  tempLength += 1; /*add null terminated char*/
  tempString.toCharArray(tempChar, tempLength);
  client.publish("esp32/BuzzerFreqRead", tempChar);

  /*Get Button Value and publish*/
  tempString = String(Buzzer1.value,DEC);
  tempLength = tempString.length();
  tempLength += 1; /*add null terminated char*/
  tempString.toCharArray(tempChar, tempLength);
  client.publish("esp32/BuzzerStateRead", tempChar);
}

//--------- Setup et Loop

void setup() {
  setup_Led     (&Led1, 0, 10000, LED_BUILTIN);                        // Led est exécutée toutes les 100ms 
  setup_Mess    (&Mess1, 1, 1000000, "bonjour: ");  
  setup_Display (&Disp1, 2, 250*1000);  // Timer 2, period 100ms
  setup_Light   (&Light1, 3, 36, 50*1000); // Timer 3, pin 9, period 10ms
  setup_Button  (&Button1, 5, 10*1000, BUTTON_PIN);
  setup_Buzzer  (&Buzzer1, 6, 10*1000, BUZZER_PIN);
  
  setup_wifi    ();
  //setup_mqtt    ();
}



void loop() {
  loop_Led(&Led1, &BoiteLed);                                        
  loop_Mess(&Mess1, &Led1, &Boite1);
  loop_Display(&Disp1, &Led1, &Light1, &Button1, &Buzzer1);
  loop_Light(&Light1, &Boite1, &BoiteLed);
  loop_Button(&Button1, &BoiteBtn_Bzz);
  loop_Buzzer(&Buzzer1, &BoiteBtn_Bzz);

  /*Check connection status*/
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  loop_publish();

  if (Serial.available())
        serialEvent();
}


/*Hanler for serial events*/
void serialEvent(){
  char inChar = (char)Serial.read();
  
  if (inChar == 'i') {
    Buzzer1.freq += (Buzzer1.freq <= 20000) ? 100 : 0;
    BoiteBtn_Bzz.flag = 2; /*New value from serial recived to buzzer*/
  }else if(inChar == 'k'){
    Buzzer1.freq -= (Buzzer1.freq > 0) ? 100 : 0;
    BoiteBtn_Bzz.flag = 2; /*New value from serial recived to buzzer*/
  }else if(inChar == 'r'){
    Buzzer1.freq = 5000;
    BoiteBtn_Bzz.flag = 2; /*New value from serial recived to buzzer*/
  }
}

/*Handler for MQTT messages communications*/
void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageTemp;
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  if (String(topic) == "esp32/buzzerFreq") {
    char tmp[messageTemp.length()];
    messageTemp.toCharArray(tmp, messageTemp.length()+1);
    int valRec = atoi(tmp);
    Serial.println(valRec);
    
    if(valRec > 0){
      Buzzer1.freq = valRec;
      BoiteBtn_Bzz.flag = 3; /*New value from MQTT recived to buzzer*/
    }
  }

  /*
   *  If a message is received on the topic esp32/buzzerValue, you check if the message is either "1" or "0". 
   */
  if (String(topic) == "esp32/buzzerValue") {
    Serial.print("Changing output to ");
    Serial.print(messageTemp);
    if(messageTemp == "1"){
      BoiteBtn_Bzz.bData = 1;
      BoiteBtn_Bzz.flag = 3; /*New value from MQTT recived to buzzer*/
    }
    else if(messageTemp == "0"){
      BoiteBtn_Bzz.bData = 0;
      BoiteBtn_Bzz.flag = 3; /*New value from MQTT recived to buzzer*/
    }
  }
}
