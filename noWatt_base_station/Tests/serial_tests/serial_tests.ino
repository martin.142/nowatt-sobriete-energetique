/**
 * @file hh_ss_test.ino
 * @author your name (you@domain.com)
 * @brief Fichier de test pour fonctionement de hardware serial
 * et software serial. L'objectif c'est d'avoir plusieures port
 * uart disponibles sur la carte
 * @version 0.1
 * @date 2022-07-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <HardwareSerial.h>
#include <SoftwareSerial.h>

/**/
HardwareSerial mySerial(1);

void setup(){
    Serial.begin(9600);
    mySerial.begin(9600, SERIAL_8N1, 12, 13);
    Serial.println("Serail ports initialized");
}

void loop(){
  mySerial.println("Mensaje de prueba");
  while(mySerial.available()) {
    Serial.print((char)mySerial.read());
  }
  Serial.println(" ");
  delay(1000);
}