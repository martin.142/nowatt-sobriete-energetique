/**
 * @file t_sleep_and_display.ino
 * @author Martin Eduardo ACOSTA CORRAL (martin.acostaec@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-07-19
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Arduino.h>
#include <WiFi.h>
#include <PubSubClient.h>

//Libraries for GSM module
#include <HardwareSerial.h>
#include <sim800l_sms.h>

//Libraries for linky protocol functions
#include <HardwareSerial.h>
#include <linky_protocol.h>

/*DEFINE for Display*/
#define SCREEN_WIDTH 128    // OLED display width, in pixels
#define SCREEN_HEIGHT 64    // OLED display height, in pixels
#define OLED_RESET     16   // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C // See datasheet for Address; 0x3C for 128x64, 0x3D for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

/*Used pins*/
#define BUTTON_PIN  23
#define BUZZER_PIN  17
#define PIN_SDA     4
#define PIN_SCL     15

/*Number of timers availables*/
#define MAX_WAIT_FOR_TIMER 15

/*Analog port configuration*/
#define ANALOG_MAXVAL     4096
#define PWM_FREQ_BASE          5000
#define PWM_LEDCHANNEL    0
#define PWM_BUZZERCHANNEL 1
#define PWM_RESOLUTION    8   //0 - 255

//GSM Definitions
#define GSM_TX_PIN 12
#define GSM_RX_PIN 13
HardwareSerial gsmSerialGlobal(1);

//LINKY Definitions
#define LINKY_TX_PIN 33 //not used
#define LINKY_RX_PIN 25
HardwareSerial linkySerialGlobal(2);

/* WIFI and MQTT Global Declaratins*/
const char* ssid = "prueba_nowatt";
const char* password = "martinacosta";

/* MQTT global declarations*/
const char* mqtt_server = "192.168.170.256";
WiFiClient espClient;
PubSubClient mqtt_client(espClient);
long lastMsg = 0;
char msg[50];
int  value = 0;

/*Number of times the ESP32 wakes up from a sleep mode*/
RTC_DATA_ATTR int bootCount = 0;

/**
 * @briefwaitFor is a non blocking function that returns the number periods
 * passed science the last function call for the specified timer. 
 * 
 * @param timer  No. timer to use
 * @param period  Period to thest
 * @return int Number of periods elapsed since last call
 */
int waitFor(int timer, unsigned long period){
  static unsigned long waitForTimer[MAX_WAIT_FOR_TIMER];  // il y a autant de timers que de tâches périodiques
  unsigned long newTime = micros() / period;              // numéro de la période modulo 2^32 
  int delta = newTime - waitForTimer[timer];              // delta entre la période courante et celle enregistrée
  if ( delta < 0 ) delta = 1 + newTime;                   // en cas de dépassement du nombre de périodes possibles sur 2^32 
  if ( delta ) waitForTimer[timer] = newTime;             // enregistrement du nouveau numéro de période
  return delta;
}

/**
 * @brief Enter ESP32 in deep_sleep mode
 * If no wake up method was configured, ESP32 will sleep
 * permanently
 * 
 */
void deep_sleep()
{
  Serial.println("Entring deep sleep mode");
  Serial.flush();
  esp_deep_sleep_start();
}

/**
 * @brief Enter ESP32 in deep_sleep mode
 * If no wake up method was configured, ESP32 will sleep
 * permanently
 * 
 */
void light_sleep()
{
  Serial.println("Entring light sleep mode");
  Serial.flush();
  esp_light_sleep_start();
}

/**
 * Déclaration structure des tâches au debut à fin d'éviter l'erreur INVALID USE OF INCOMPLET TYPE
 */
struct Boite_s{
  int flag;
  unsigned int bData;
};

struct Led_s {
  int timer;              // numéro du timer pour cette tâche utilisé par WaitFor
  unsigned long period;   // periode de clignotement
  int pin;                // numéro de la broche sur laquelle est la LED
  int etat;               // etat interne de la led
  int duty;
}; 

struct Mess_s {
  int timer;              // numéro de timer utilisé par WaitFor
  unsigned long period;   // periode d'affichage
  char mess[20];
} ; 

struct Ligth_s{
  int timer;            
  int period;
  int pin;
  unsigned int val;
};

struct Display_s{
  int timer;
  int period;
  String stringData;    //Data a afficher sur plusieures lignes. 21 characters par lige
};

struct Button_s{
  int timer;
  int period;
  int pin;
  int value;
};

struct Buzzer_s{
  int timer;
  int period;     //Periode entre appels a la fonction
  int pin;
  int value;
  int freq;       //Frenquece du signal en sortie
};

struct GSM_s{
  int timer;
  int period;
  HardwareSerial *gsm_serial;
  int baud;
  int rx_pin;
  int tx_pin;
  int isInitialized;
  String sender;
  String data;
};

struct WIFI_s{
  int timer;
  int period;
  int initialized;
  int connected;
  char *ssid;
  char *pass;
};

struct MQTT_s{
  int timer;
  int period;
  char *server;
  int port;
  int connected;
};

struct Linky_s {
  int timer;
  int period;
  HardwareSerial *linky_serial;
  String allData;
  int current;
  int voltage;
  int power;
  int initialized;
};

/**
 * ----------- Déclaration des tâches
 */
struct Led_s Led1;
struct Mess_s Mess1;
struct Display_s Disp1;
struct Ligth_s Light1;
struct Button_s Button1;
struct Buzzer_s Buzzer1;
struct GSM_s    Gsm1;
struct WIFI_s Wifi1;
struct MQTT_s Mqtt1;
struct Linky_s Linky1;

/**
 * ----------- Déclaration des boites aux lettrs (moyen de communication inter-tâche)
 */
struct Boite_s Boite1;
struct Boite_s BoiteLed;
struct Boite_s BoiteBtn_Bzz;
struct Boite_s BoitelightAlarmSms;

/**
 * --------- Definition of setup and loop tache
 */

void setup_Led( struct Led_s * ctx, int timer, unsigned long period, byte pin) {
  ctx->timer = timer;
  ctx->period = period;
  ctx->pin = pin;
  ctx->etat = 1;
  ctx->duty = 0;

  /**
   * Configuration du canal PWM pour la led. 
   */
  ledcAttachPin(pin, PWM_LEDCHANNEL);
  ledcSetup(PWM_LEDCHANNEL, PWM_FREQ_BASE, PWM_RESOLUTION);
}

void loop_Led( struct Led_s * ctx, struct Boite_s *boiteLed) {
  if(!(waitFor(ctx->timer,ctx->period))) return;

  float tmpDutyCycle; 
  
  if(boiteLed->flag ==  1){ //Si flag = 1, Message pendants
    boiteLed->flag = 0;
    ctx->duty = boiteLed->bData;  //Lécture et stockage du message

    /**
     * Tester que duty cycle != de zero sino divition par zero. Exeption
     * Calculer le rapport cyquile de 0 - 1 avec point décimal
     */
    if(ctx->duty != 0)tmpDutyCycle = (float(ctx->duty) / (float)100);
    else tmpDutyCycle;

    /**
     * 0 -> 0% ..... 128 -> 50% ....... 255 -> 100%
     */
    ledcWrite(PWM_LEDCHANNEL, 255*tmpDutyCycle); 
  }                
}

//--------- definition de la tache Mess

/**
 * Message affiches des information sur la ligne de commande
 */
void setup_Mess( struct Mess_s * ctx, int timer, unsigned long period, const char * mess) {
  ctx->timer = timer;
  ctx->period = period;
  
  strcpy(ctx->mess, mess);
  Serial.begin(9600);     // initialisation du débit de la liaison série
}

void loop_Mess(struct Mess_s *ctx, struct Led_s *led, struct Boite_s *boite) {
  if (!(waitFor(ctx->timer,ctx->period))) return;         // sort s'il y a moins d'une période écoulée
  return;
  Serial.print(ctx->mess); // affichage du message
  Serial.print(" PWM Level: ");
  Serial.print(led->duty);
  Serial.print(" Light leve: ");
  Serial.println(boite->bData);
}


//----------- Definition de la tache light sensor 

void setup_Light(struct Ligth_s *ctx, int timer, int pin, int period){
  ctx->timer = timer;
  ctx->period = period;
  ctx->pin = pin;
}

void loop_Light(struct Ligth_s *ctx, struct Boite_s *boite, struct Boite_s *boiteLed){
  /**
   * On envoi des valuer par boite aux lettre. On ne teste pas si la valuer précédente a été lue, 
   * On n'as pas besoir d'aquiter la reception d'une donné
   */
  if ((waitFor(ctx->timer,ctx->period))){ 
    ctx->val = analogRead(ctx->pin);

    //Convertir la valeur de 0-4096 à 0% - 100%
    unsigned int valTmp = (unsigned int)(((float)(ANALOG_MAXVAL - ctx->val) / (float)ANALOG_MAXVAL) * 100);
    boite->bData = valTmp;
    if (valTmp > 90) {
      BoitelightAlarmSms.flag = 1;
    } else {
      BoitelightAlarmSms.flag = 0;
    }
  }
  
  if(boiteLed->flag == 0){  //Si != de 0. Les valeurs précedences n'ont pas encore été traités
    if ((waitFor(ctx->timer+1,ctx->period*5))){ 
      boiteLed->flag = 1; //Le flag indique à la tâche lecteur qu'il y a un donnée READY

      unsigned int valTmp = (unsigned int)(((float)(ANALOG_MAXVAL - ctx->val) / (float)ANALOG_MAXVAL) * 100);
      boiteLed->bData = valTmp;
      if(boiteLed->bData == 0) boiteLed->bData = 1; //Eviter que PWM égal a 0
    }
  }
}


// ----------- definition de la tache display

void setup_Display(struct Display_s * ctx, int timer, int period){
  ctx->timer = timer;
  ctx->period = period;
  ctx->stringData = "String Test";
  
  Wire.begin(PIN_SDA, PIN_SCL); // Initialisation communication I2C
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
  }
  display.display();
  delay(500); // Pause for 2 seconds
  display.clearDisplay();
}


String alive = "-";
void loop_Display(struct Display_s *ctx, struct Led_s *led, struct Ligth_s *light, struct Button_s *button, struct Buzzer_s *buzzer){
  if (!(waitFor(ctx->timer,ctx->period))) return;   // return si la periode n'est pas encore passé

  /**
   * Transforme la valeur analogic en pourcentage 0 - 100%
   */
  unsigned int rawToPrc = (unsigned int)(((float)(ANALOG_MAXVAL - light->val) / (float)ANALOG_MAXVAL) * 100);

  /**
   * Construction du string à affciher.
   * Tout le string est stocké dans l'attribut du Display.
   * 
   * la fonction appendLine permet de remplir une linge avec de SPACE pour 
   * avoir l'inforrmation suivant dans la ligne suivante.
   */

  //BUZZER STATUS
  ctx->stringData = "GMS";
  ctx->stringData += (Gsm1.isInitialized == 1) ? "*" : "-";

  //BUZZER STATUS
  ctx->stringData += " WIFI";
  ctx->stringData += (Wifi1.connected == 1) ? "*" : "-";

  //BUZZER STATUS
  ctx->stringData += " MQTT";
  ctx->stringData += (Mqtt1.connected == 1) ? "*" : "-";

  ctx->stringData += "  ";
  ctx->stringData += alive;
  alive = (alive.indexOf('-') != -1)? " " : "-";

  appendLine(ctx->stringData);

  //BUZZER STATUS
  ctx->stringData += "Boot:";
  ctx->stringData += bootCount;

  //Light LEVEL
  ctx->stringData += " Light:";
  ctx->stringData += String(rawToPrc, DEC);
  ctx->stringData += "%";

  appendLine(ctx->stringData);

  //BTN VALUE
  ctx->stringData += "ButtonVl: ";
  ctx->stringData += button->value;

  appendLine(ctx->stringData);

  //BTN VALUE
  ctx->stringData += "I:";
  ctx->stringData += Linky1.current;

  ctx->stringData += " V:";
  ctx->stringData += Linky1.voltage;

  ctx->stringData += " S:";
  ctx->stringData += Linky1.power;

  appendLine(ctx->stringData);

  //BUZZER STATUS
  ctx->stringData += Gsm1.sender;

  appendLine(ctx->stringData);

  ctx->stringData += Gsm1.data;
  
  int stringlen = ctx->stringData.length() + 1; //Obtenir la longeur du string
  
  /**
   * Transfomrer le chaine de type string en char*. 
   */
  char tmp[stringlen];                          
  ctx->stringData.toCharArray(tmp, stringlen); // Cast string to char*
  tmp[stringlen] = 0;
  
  testdrawtext(tmp);
}


void testdrawtext(char* text) {
  //21 characters par ligne
  display.clearDisplay();
  display.setTextSize(0);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  
  display.setCursor(0, 0);     // Start at top-left corner
  display.cp437(true);         // Use full 256 char 'Code Page 437' font
  display.write(text);
  display.display();
}

// Concatenate SPACE to pass to next line
void appendLine(String &str){
  for(int i = (str.length() % 21); i < 21 ; i++){
    str += ' ';
  }
}

// -------- Tache button

/**
 * Handler du interruption boutton
 */
void IRAM_ATTR isr() {
  Serial.println("BUTTON INTERRUPTION RECIVED");
}

void setup_Button(struct Button_s *button, int timer, int period, int pin){
  button->timer  = timer;
  button->period = period;
  button->pin    = pin;
  button->value  = 0; //Init value a zero
  
  pinMode(button->pin, INPUT_PULLUP); //l'option INPUT_PULLUP est nécessaire à cause du mode de connection du boutton

  /**
   * Déclaration d'une interruption pour le GPIO boutton. 
   * la fonction appelé sera isr() et FALLING indique le déclanchement
   * de l'interruption au front décendante du signal
   */
  attachInterrupt(button->pin, isr, FALLING); 
}

void publishButtonVal(){
  char tempChar[8];
  String tempString;
  int tempLength;
  
    /*Get Button Value and publish it*/
  tempString = String(Button1.value,DEC);
  tempLength = tempString.length();
  tempLength += 1; /*Take care of null terminated char*/
  tempString.toCharArray(tempChar, tempLength);

  // Publish buttonValue sur topic = esp32/ButtonState
  mqtt_client.publish("esp32/ButtonState", tempChar);
}

void loop_Button(struct Button_s *button, struct Boite_s *boite){
  //if (boite->flag == 1) return; //Valeur pas encore lu
  if (!(waitFor(button->timer,button->period))) return;

  button->value = 1 - digitalRead(button->pin);

  /*Send the message and publish the message when value changed*/
  static int lastVal;
  if(lastVal != button->value){
    boite->flag = 1;
    boite->bData = button->value;
    lastVal = button->value;

    publishButtonVal();
  }
}


//--------- Définition buzzer methodes

void setup_Buzzer(struct Buzzer_s *buzzer, int timer, int period, int pin){
  buzzer->timer   = timer;
  buzzer->period  = period;
  buzzer->pin     = pin;
  buzzer->freq    = 5000;
  buzzer->value   = 0;

  //Configuration du signal PWM  Pour buzzer
  ledcAttachPin(buzzer->pin, PWM_BUZZERCHANNEL);
  ledcSetup(PWM_BUZZERCHANNEL, buzzer->freq, PWM_RESOLUTION);
}

void loop_Buzzer(struct Buzzer_s *buzzer, struct Boite_s *boiteButton){
  /*Static variables, the value is the same at every function call*/
  static int lastVal;
  static int lastFrq;

  /*
   * Check if any message is recived or the time is elapsed
   * 1 -> Message sent from button read function
   * 2 -> Message sent from serial port value recived
   * 3 -> Message sent from MQTT publisher
   */
  if(boiteButton->flag == 0) return; /*No messaged Recived. Return*/
  if(!(waitFor(buzzer->timer, buzzer->period))) return;
  
  buzzer->value = boiteButton->bData;

  /*Detect and execute the command only if the buzzer val or freq has changed*/
  if(lastVal != buzzer->value or lastFrq != buzzer->freq){
    if(buzzer->value){
      ledcWriteTone(PWM_BUZZERCHANNEL, buzzer->freq);
    }else{
      ledcWriteTone(PWM_BUZZERCHANNEL, 0);
    }

    /*Update Values*/
    lastVal = buzzer->value;
    lastFrq = buzzer->freq;
  }

  /*Acknowlege the reseption of the data*/
  boiteButton->flag = 0;
}


void setup_wifi(struct WIFI_s *ctx, int timer, int period, const char *ssid_param, const char *pass_param) {
  
  ctx->timer = timer;
  ctx->period = period;
  ctx->initialized = 0;
  ctx->connected = 0;
  ctx->ssid = (char *)ssid_param;
  ctx->pass = (char *)pass_param;

  /*We start by connecting to a WiFi network*/
  Serial.print("Initializing WIFI, connecting to: ");
  Serial.println(ctx->ssid);

  // delete old config
  WiFi.disconnect(true);

  // Examples of different ways to register wifi events
  WiFi.onEvent(WiFiEvent);
  //WiFi.onEvent(WiFiGotIP, WiFiEvent_t::SYSTEM_EVENT_STA_GOT_IP);
  
/*   WiFiEventId_t eventID = WiFi.onEvent([](WiFiEvent_t event, WiFiEventInfo_t info){
    Serial.print("WiFi lost connection. Reason: ");
    Serial.println(info.disconnected.reason);
  }, WiFiEvent_t::SYSTEM_EVENT_STA_DISCONNECTED); */
  
  // Station Mode : ESP32 connects to a acces point
  WiFi.mode(WIFI_STA);

  //Begin connection
  WiFi.begin(ssid, password);

  /**
   * @brief wifi handler will let us know wheter a connection is revied
   * or a current connection is lost
   * 
   */
}


/* MQTT methods */

void reconnect_mqtt()
{
  /*Connection to MQTT broker*/
  
/*Loop until we're reconnected*/
  Serial.print("Attempting MQTT connection...");
  
  // Attempt to connect
  if (mqtt_client.connect("ESP8266Client")) {
    Serial.println("connected");
    
    /*Subscribe topics. ESP32 will listen to these topics*/
    mqtt_client.subscribe("esp32/buzzerFreq");
    mqtt_client.subscribe("esp32/buzzerValue");
  } else {
    Serial.print("failed, rc=");
    Serial.print(mqtt_client.state());
    Serial.println(" try again in 5 seconds");
    
    // Wait 5 seconds before retrying
    //delay(5000);
  }
}

void setup_mqtt(struct MQTT_s *ctx, int timer, int period, const char *server, int port)
{

  ctx->timer = timer;  
  ctx->period = period;
  ctx->server = (char *)server;
  ctx->port = port;
  ctx->connected = 0;

  mqtt_client.setServer(ctx->server, ctx->port);
  mqtt_client.setCallback(callback);
}

void loop_mqtt(struct MQTT_s *ctx)
{
  /*This function must be called periodically to check the
  server state and call hanlers in case of any server activity*/
  if (ctx->connected == 1)
    mqtt_client.loop();

  if(!(waitFor(ctx->timer, ctx->period))) 
    return;
  
  /*Try to connect to mqtt if disconnected and wifi initialized*/
  if (!mqtt_client.connected() and Wifi1.connected == 1) {
    ctx->connected = 0;
    reconnect_mqtt();
    return; 
  } else if (mqtt_client.connected() and Wifi1.connected == 1) {
    ctx->connected = 1;
  } else {
    ctx->connected = 0;
    return;
  }
  
  /*
   * Publisher will publish all the information showed  
   * on the esp32 screen. Light Level, PWM Level, Button state
   * Buzzer Freq et Buzzer State
   */
  char tempChar[8];
  String tempString;
  int tempLength;
  

  /*Get Light Level and publish*/
  tempString = String(Light1.val,DEC);
  tempLength = tempString.length();
  tempLength += 1; /*add null terminated char*/
  tempString.toCharArray(tempChar, tempLength);
  mqtt_client.publish("esp32/LightLevel", tempChar);

  /*Get PWM Level and publish*/
  tempString = String(Led1.duty,DEC);
  tempLength = tempString.length();
  tempLength += 1; /*add null terminated char*/
  tempString.toCharArray(tempChar, tempLength);
  mqtt_client.publish("esp32/PWMLevel", tempChar);

  /*Get Buzzer Freqauency Read and publish*/
  tempString = String(Buzzer1.freq,DEC);
  tempLength = tempString.length();
  tempLength += 1; /*add null terminated char*/
  tempString.toCharArray(tempChar, tempLength);
  mqtt_client.publish("esp32/BuzzerFreqRead", tempChar);

  /*Get Button Value and publish*/
  tempString = String(Buzzer1.value,DEC);
  tempLength = tempString.length();
  tempLength += 1; /*add null terminated char*/
  tempString.toCharArray(tempChar, tempLength);
  mqtt_client.publish("esp32/BuzzerStateRead", tempChar);
}


//Setup and loop of GSM task

void setup_GSM(struct GSM_s *ctx, int timer, int period, int baud, int rx_pin, int tx_pin)
{
  
  ctx->timer = timer;
  ctx->period = period;
  ctx->baud = baud;
  ctx->rx_pin = rx_pin;
  ctx->tx_pin = tx_pin;
  ctx->isInitialized = 0;
  ctx->gsm_serial = &gsmSerialGlobal;
}

void loop_GSM(struct GSM_s *ctx)
{
  int ret;

  if(!(waitFor(ctx->timer, ctx->period))) return;

  if (SMS.initialized() == 0) {
    if(SMS.setup(ctx->gsm_serial, ctx->baud, ctx->rx_pin, ctx->tx_pin) == 1) {
      ctx->isInitialized = 1;
    }
    return;
  }

  SMS.loop();

  ret = SMS.read(ctx->sender, ctx->data);

  if( ret == 1) {
    Serial.print("SMS recived from: ");
    Serial.print(ctx->sender);
    Serial.print(" : ");
    Serial.println(ctx->data);

    if (ctx->data.indexOf("deep sleep") != -1)
      deep_sleep();

    if (ctx->data.indexOf("light sleep") != -1)
      light_sleep();
	} else if (ret == -1) {

  }

  if(!(waitFor(MAX_WAIT_FOR_TIMER-1, 25000000))) return;

  String numberDest("+33749187590");
  String Alarm("Light consomation too high.");

  if (BoitelightAlarmSms.flag) {
    if (SMS.send(numberDest, Alarm) == -1)
      return;
    BoitelightAlarmSms.flag = 0;
  }
    
}

// Linky protocoles methodes

void setup_linky(struct Linky_s *ctx, int timer, int period)
{
  ctx->timer = timer;
  ctx->period = period;
  ctx->linky_serial = &linkySerialGlobal;
  ctx->allData = "";
  ctx->initialized = 0;
  ctx->current = -1;
  ctx->voltage = -1;
  ctx->power = -1;

  if(LINKY.setup(ctx->linky_serial, 9600, LINKY_RX_PIN, LINKY_TX_PIN) == -1)
    return;

  ctx->initialized = 1;
  return;
}

void loop_linky(struct Linky_s *ctx)
{
  if(!(waitFor(ctx->timer, ctx->period))) return;

  LINKY.readData();

  ctx->allData = LINKY.getAllData();
  ctx->current = LINKY.getCurrent();
  ctx->voltage = LINKY.getVoltage();
  ctx->power   = LINKY.getPower();
} 


//--------- Setup et Loop

void setup()
{
  //setup_Led     (&Led1, 0, 10000, LED_BUILTIN);                        // Led est exécutée toutes les 100ms 
  setup_Mess    (&Mess1, 1, 1000000, "bonjour: ");  
  setup_Display (&Disp1, 2, 250*1000);  // Timer 2, period 100ms
  setup_Light   (&Light1, 3, 36, 50*1000); // Timer 3, pin 9, period 10ms
  setup_Button  (&Button1, 5, 10*1000, BUTTON_PIN);
  loop_Display(&Disp1, &Led1, &Light1, &Button1, &Buzzer1);
  //setup_Buzzer  (&Buzzer1, 6, 10*1000, BUZZER_PIN);
  setup_GSM     (&Gsm1, 6, 500000, 9600, GSM_RX_PIN, GSM_TX_PIN);
  setup_wifi    (&Wifi1, 7, 1000000, ssid, password);
  setup_mqtt    (&Mqtt1, 8, 1000000, mqtt_server, 1883);
  //setup_linky   (&Linky1, 9, 250000);

  ++bootCount;
  Serial.println(" ");
  Serial.print("Boot count: ");
  Serial.println(bootCount);

  print_wakeup_reason();

  esp_sleep_enable_timer_wakeup(20 * 1000000);
}

void loop() {
  //loop_Led(&Led1, &BoiteLed);                                        
  loop_Mess(&Mess1, &Led1, &Boite1);
  loop_Display(&Disp1, &Led1, &Light1, &Button1, &Buzzer1);
  loop_Light(&Light1, &Boite1, &BoiteLed);
  loop_Button(&Button1, &BoiteBtn_Bzz);
  //loop_Buzzer(&Buzzer1, &BoiteBtn_Bzz);
  loop_GSM(&Gsm1);
  //loop_linky(&Linky1);

  if (Wifi1.connected)
    loop_mqtt(&Mqtt1);

  if (Serial.available())
    serialEvent();
}


/**********************************************************/
/*                   EVENT HANDLERS                        */
/**********************************************************/


/**
 * @brief Called when the Seria object
 * recives any information. Called by the loop function
 * 
 */
void serialEvent(){

  String data;
  String number = "+33749187590";

  delay(50);

  while (Serial.available()){
    data += (char)Serial.read();
  }

  if (data.indexOf("deep sleep") != -1) {
    deep_sleep();
  } else if (data.indexOf("light sleep") != -1) {
    light_sleep();
  } else if (data.indexOf("SEND_SMS=-") != -1) {
		String smsData = data.substring(data.indexOf("=-") + 2);
		Serial.print("Sending SMS to : ");
		Serial.print(number);
		Serial.print(" : ");
		Serial.println(smsData);

		SMS.send(number, smsData);
	} else if (data.indexOf("SEND_SMS_TO=+") != -1 and data.indexOf("=-") != -1) {
		String smsNumber = data.substring(data.indexOf("=+") + 1 , data.indexOf("=-"));
		String smsData   = data.substring(data.indexOf("=-") + 2);
		Serial.print ("Sending SMS to: ");
		Serial.print(smsNumber);
		Serial.print(" Data: ");
		Serial.println(smsData);

		SMS.send(smsNumber, smsData);
	} else if (data.indexOf("DELETE_ALL_SMS") != -1) {
		Serial.println("Deleting all messages");
		int nbSmsDel = SMS.delete_all_SMS();
		if (nbSmsDel == -1)
			Serial.println("ERROR Deleting sms");
		Serial.print("SMS deleted: ");
		Serial.println(nbSmsDel);
	} else {
		Serial.print("INCORRECT INPUT\n");
	}

}

/**
 * @brief Handler for MQTT messages communications
 * 
 * @param topic Topic recived
 * @param message Data of the topic
 * @param length Nomber of data bytes
 */
void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageTemp;
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  if (String(topic) == "esp32/buzzerFreq") {
    char tmp[messageTemp.length()];
    messageTemp.toCharArray(tmp, messageTemp.length()+1);
    int valRec = atoi(tmp);
    Serial.println(valRec);
    
    if(valRec > 0){
      Buzzer1.freq = valRec;
      BoiteBtn_Bzz.flag = 3; /*New value from MQTT recived to buzzer*/
    }
  }

  /*
   *  If a message is received on the topic esp32/buzzerValue, you check if the message is either "1" or "0". 
   */
  if (String(topic) == "esp32/buzzerValue") {
    Serial.print("Changing output to ");
    Serial.print(messageTemp);
    if(messageTemp == "1"){
      BoiteBtn_Bzz.bData = 1;
      BoiteBtn_Bzz.flag = 3; /*New value from MQTT recived to buzzer*/
    }
    else if(messageTemp == "0"){
      BoiteBtn_Bzz.bData = 0;
      BoiteBtn_Bzz.flag = 3; /*New value from MQTT recived to buzzer*/
    }
  }
}

/**
 * @brief Method to print the reason by which ESP32
 * has been awaken from sleep
 */
void print_wakeup_reason(){
	esp_sleep_wakeup_cause_t wakeup_reason;
	
	wakeup_reason = esp_sleep_get_wakeup_cause();

	switch(wakeup_reason)
	{
		case ESP_SLEEP_WAKEUP_UNDEFINED 		: Serial.println("In case of deep sleep, reset was not caused by exit from deep sleep"); break;
		case ESP_SLEEP_WAKEUP_ALL 				: Serial.println("Not a wakeup cause"); break;
		case ESP_SLEEP_WAKEUP_EXT0 				: Serial.println("Wakeup caused by external signal using RTC_IO"); break;
		case ESP_SLEEP_WAKEUP_EXT1 				: Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
		case ESP_SLEEP_WAKEUP_TIMER 			: Serial.println("Wakeup caused by timer"); break;
		case ESP_SLEEP_WAKEUP_TOUCHPAD 			: Serial.println("Wakeup caused by touchpad"); break;
		case ESP_SLEEP_WAKEUP_ULP 				: Serial.println("Wakeup caused by ULP program"); break;
		case ESP_SLEEP_WAKEUP_GPIO 				: Serial.println("Wakeup caused by GPIO (light sleep only) "); break;
		case ESP_SLEEP_WAKEUP_UART 				: Serial.println("Wakeup caused by UART (light sleep only) "); break;
		case ESP_SLEEP_WAKEUP_WIFI 				: Serial.println("Wakeup caused by WIFI (light sleep only) "); break;
		case ESP_SLEEP_WAKEUP_COCPU 			: Serial.println("Wakeup caused by COCPU int. "); break;
		case ESP_SLEEP_WAKEUP_COCPU_TRAP_TRIG	: Serial.println("Wakeup caused by COCPU crash. "); break;
		case ESP_SLEEP_WAKEUP_BT 				: Serial.println("Wakeup caused by BT (light sleep only) "); break;
		default : Serial.printf("First Wakeup, reason: %d\n",wakeup_reason); break;
	}
}


/**
 * @brief WIFI event handler, every event will call
 * this function
 * 
 * @param event 
 */
void WiFiEvent(WiFiEvent_t event) {
  //Serial.printf("[WiFi-event] event: %d\n", event);

  switch (event) {
    case SYSTEM_EVENT_WIFI_READY: 
      Serial.println("WiFi interface ready");
      break;
    case SYSTEM_EVENT_SCAN_DONE:
      Serial.println("Completed scan for access points");
      break;
    case SYSTEM_EVENT_STA_START:
      Serial.println("WiFi client started");
      break;
    case SYSTEM_EVENT_STA_STOP:
      Serial.println("WiFi clients stopped");
      break;
    case SYSTEM_EVENT_STA_CONNECTED:
      Serial.println("Connected to access point"); //
      break;
    case SYSTEM_EVENT_STA_DISCONNECTED: //Disconnected from a network
      //Serial.println("Disconnected from WiFi access point");
      //Serial.println("");
      Wifi1.connected = 0;
      WiFi.begin(Wifi1.ssid, Wifi1.pass);
      break;
    case SYSTEM_EVENT_STA_AUTHMODE_CHANGE:
      Serial.println("Authentication mode of access point has changed");
      break;
    /*CASE ALREADY HANDLED BY THE NEXT FUNCTION*/
    //case SYSTEM_EVENT_STA_GOT_IP:
    //  Serial.print("Obtained IP address: "); 
    //  Serial.println(WiFi.localIP());
    //  Wifi1.connected = 1;
    //  break;
    case SYSTEM_EVENT_STA_LOST_IP:
      Serial.println("Lost IP address and IP address is reset to 0");
      break;
    case SYSTEM_EVENT_STA_WPS_ER_SUCCESS:
      Serial.println("WiFi Protected Setup (WPS): succeeded in enrollee mode");
      break;
    case SYSTEM_EVENT_STA_WPS_ER_FAILED:
      Serial.println("WiFi Protected Setup (WPS): failed in enrollee mode");
      break;
    case SYSTEM_EVENT_STA_WPS_ER_TIMEOUT:
      Serial.println("WiFi Protected Setup (WPS): timeout in enrollee mode");
      break;
    case SYSTEM_EVENT_STA_WPS_ER_PIN:
      Serial.println("WiFi Protected Setup (WPS): pin code in enrollee mode");
      break;
    case SYSTEM_EVENT_AP_START:
      Serial.println("WiFi access point started");
      break;
    case SYSTEM_EVENT_AP_STOP:
      Serial.println("WiFi access point  stopped");
      break;
    case SYSTEM_EVENT_AP_STACONNECTED:
      Serial.println("Client connected");
      break;
    case SYSTEM_EVENT_AP_STADISCONNECTED:
      Serial.println("Client disconnected");
      break;
    case SYSTEM_EVENT_AP_STAIPASSIGNED:
      Serial.println("Assigned IP address to client");
      break;
    case SYSTEM_EVENT_AP_PROBEREQRECVED:
      Serial.println("Received probe request");
      break;
    case SYSTEM_EVENT_GOT_IP6:
      Serial.println("IPv6 is preferred");
      break;
    case SYSTEM_EVENT_ETH_START:
      Serial.println("Ethernet started");
      break;
    case SYSTEM_EVENT_ETH_STOP:
      Serial.println("Ethernet stopped");
      break;
    case SYSTEM_EVENT_ETH_CONNECTED:
      Serial.println("Ethernet connected");
      break;
    case SYSTEM_EVENT_ETH_DISCONNECTED:
      Serial.println("Ethernet disconnected");
      break;
    case SYSTEM_EVENT_ETH_GOT_IP:
      Serial.println("Obtained IP address");
      break;
    default: break;
  }
}

/**
 * @brief Handler for Wifi connection at the moment when an IP is
 * assigned by the acces point to the ESP32
 * 
 * @param event 
 * @param info 
 */
void WiFiGotIP(WiFiEvent_t event, WiFiEventInfo_t info)
{
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(IPAddress(info.got_ip.ip_info.ip.addr));
    Wifi1.connected = 1;
}
