/**
 * @file noWatt.ino
 * @author Martin Eduardo ACOSTA CORRAL (martin.acostaec@gmail.com)
 * @brief Test multiple functionalities on the base station
 * 
 * 	- Linky reader using the custom library
 *  - Send and recive sms (execute commands and alarsm)
 *  - Get internet time only once and use RTC timer to keep it
 *  - Detect BLE devices (only one, IN DEVELOPEMENT)
 *  - Connected to a MQTT (Can be initialized But no interaction)
 * 
 * NOT FULLY FUNCTIONAL THERE ARE BUG.
 * 
 * This same template has been copied to create the main.ino
 * 
 * @version 0.1
 * @date 2022-07-26
 * 
 * @copyright Copyright (c) 2022
 * 
 */


//
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Arduino.h>
#include <PubSubClient.h>

// Wifi and time functions
#include <WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

// Libraries for GSM module
#include <HardwareSerial.h>
#include <sim800l_sms.h>

// Libraries for linky protocol functions
#include <linky_protocol.h>

// BLE Includes
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>
#include "esp_bt.h"

// Time includes
#include <sys/time.h>
#include <Ticker.h>

#define CURRENT_VERSION "0.1"
#define FULL  1
#define EMPTY 0
#define SLEEP_TIME (20000000) // 20 S
#define INTERRUPT_TIMER_TIME (5 * 60 * 1000000) //10 minutes (1 * 60 * 1000000) //10 minutes

#define NB_MAX_COMMANDS	10
#define CMD_SRC_SERIAL	0
#define CMD_SRC_SMS		1
#define CMD_SRC_MQTT	2
#define CMD_SRC_BLE		3

String number = "+33749187590";
String globalAlarms;

/* DEFINE for Display */
#define SCREEN_WIDTH	128		// OLED display width, in pixels
#define SCREEN_HEIGHT	64		// OLED display height, in pixels
#define OLED_RESET		16		// Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS	0x3C	// See datasheet for Address; 0x3C for 128x64, 0x3D for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
#define NB_SCREENS 9

/* Used pins */
//#define BUTTON_PIN		23
#define BUTTON_PIN		0
#define BUZZER_PIN		17
#define PIN_SDA			4
#define PIN_SCL			15
#define LINKY_TX_PIN	25 // not used
#define LINKY_RX_PIN	14 
#define GSM_TX_PIN		12
#define GSM_RX_PIN		13

/* Number of timers availables */
#define MAX_WAIT_FOR_TIMER 20

/* Analog port configuration */
#define ANALOG_MAXVAL     4096
#define PWM_FREQ_BASE     5000
#define PWM_LEDCHANNEL    0
#define PWM_BUZZERCHANNEL 1
#define PWM_RESOLUTION    8   // 0 - 255

// GSM Definitions
HardwareSerial gsmSerialGlobal(1);

// LINKY Definitions
HardwareSerial linkySerialGlobal(2);
#define LINKY_HISTORY_SIZE 100
#define LINKY_MEAN_SIZE	50
#define	NB_STATS_VALUES	20

// BLE definitions
#define ENDIAN_CHANGE_U16(x) ((((x)&0xFF00) >> 8) + (((x)&0xFF) << 8))
#define SCANTIME 1 // In seconds

//Touch defines
#define TOUCH_PIN_0 27
#define SHORT_TOUCH_MIN_MS 25
#define SHORT_TOUCH_MAX_MS 1000

/* WIFI and MQTT Global Declaratins */
const char* ssid 		= "prueba_nowatt";
const char* password 	= "martinacosta";

/* MQTT global declarations */
const char* mqtt_server = "192.168.170.256";
WiFiClient espClient;
PubSubClient mqtt_client(espClient);
long lastMsg = 0;
char msg[50];
int  value = 0;

//Multicore tasks cores definitions
TaskHandle_t coreZero;
TaskHandle_t coreOne;

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

/* Simple interrupt declarations */
hw_timer_t *My_timer = NULL;

/* Function declaration */
void IRAM_ATTR onTimer(); int timerIrqFlag;


/**
 * @brief Number of times the ESP32 wakes up from a sleep mode
 * PRESERVED VARIABLES AFTR A DEEPS SLEEP
 * 
 * RTC_DATA_ATTR Stands for data that will be store in the RTC memroy
 * RTC memroy remains powered when the ESP32 passes en mode deep sleep,
 * there is 8KB available
 * 
 */
RTC_DATA_ATTR int bootCount = 0;
RTC_DATA_ATTR int isTimeSet = 0;


/**
 * @brief Declaration of structure to statics calulation
 */

struct statistics {
	int average;
	int max = 0;
	int min = 0xFFFFFFFF;
};

struct measurement_data {
	struct tm timestamp;
	int value;
};

struct measurement_historic {
	struct measurement_data measurements[LINKY_HISTORY_SIZE];
	struct statistics stats;
	char unit[8];
	int nbElements;
	int index;
};

struct measurement_statistics {
	struct measurement_data measurements[LINKY_MEAN_SIZE];
	struct statistics stats;
	char unit[8];
	int nbElements;
	int index;
};

/**********************************************************************/

/* struct measurement_historic {
	
	struct measurement_data {
		struct tm timestamp;
		int value;
	} measurements[LINKY_HISTORY_SIZE];

	struct statistics {
		int average;
		int max;
		int min;
	} stats;

	char unit[8];
	int nbElements;
	int index;
};

struct measurement_statistics {

	struct measurement_data {
		struct tm timestamp;
		int value;
	} measurements[LINKY_MEAN_SIZE];

	struct statistics {
		int average;
		int max;
		int min;
	} stats;

	char unit[8];
	int nbElements;
	int index;
}; */

/**
 * Déclaration structure des tâches au debut à fin d'éviter l'erreur INVALID USE OF INCOMPLET TYPE
 */
struct Boite_s{
	int flag;
	unsigned int bData;
	int nextNotification;
};

struct Led_s {
	int timer;              // numéro du timer pour cette tâche utilisé par WaitFor
	unsigned long period;   // periode de clignotement
	int pin;                // numéro de la broche sur laquelle est la LED
	int etat;               // etat interne de la led
	int duty;
}; 

struct Mess_s {
	int timer;              // numéro de timer utilisé par WaitFor
	unsigned long period;   // periode d'affichage
	char mess[20];
} ; 

struct Ligth_s{
	int timer;            
	int period;
	int pin;
	unsigned int val;
};

struct Display_s{
	int timer;
	int period;
	int curScreen;		//Current screen
	int optionScreen;	//Option for the current screen
	int mainReturnCount;//Counter to return to the main screen

	String stringData;	//Data a afficher sur plusieures lignes. 21 characters par lige
	String aliveStr;	//String that changes at every iteration of displayLoop to indicate alive
};

struct Button_s{
	int timer;
	int period;
	int pin;
	int value;
	int lastVal;
	
	int pressCount;	//Count of iterations where the button were pressed
	int shortPress;	//Signal to indicate a short button press
	int longPress;	//Signal to indicate a ling button press
	int mustRelease;//Signal to block multiple long press singnals
};

struct GSM_s{
	int timer;
	int period;
	int baud;
	int rx_pin;
	int tx_pin;
	int isInitialized;

	String sender;
	String data;

	HardwareSerial *gsm_serial;
};

struct WIFI_s{
	int timer;
	int period;
	int initialized;
	int connected;
	char *ssid;
	char *pass;
};

struct MQTT_s{
	int timer;
	int period;
	char *server;
	int port;
	int connected;
};

struct Linky_s {
	int timer;
	int period;
	int connected;	//is noWatt connected to Linky
	int newData;	// Alternate between 0 and 1 to indicate each time new data arrive

	int current;
	int voltage;
	int power;
	int statsCount;

	int maxLimit = -1;
	int maxLimitAlarmOnceDay = 0;

	HardwareSerial *linky_serial;
	String allData;

	struct measurement_statistics statsVoltage 	= {.unit = {'V', 0}};
	struct measurement_statistics statsCurrent 	= {.unit = {'A', 0}};
	struct measurement_statistics statsPower 	= {.unit = {'V', 'A', 0}};

	struct measurement_historic historicVoltage = {.unit = {'V', 0}};
	struct measurement_historic historicCurrent = {.unit = {'A', 0}};
	struct measurement_historic historicPower 	= {.unit = {'V', 'A', 0}};
};

struct BLE_s {
	int timer;
	int period;
	int initialized;
	BLEScan *pBLEScan;

	String deviceName;
	String manufacturerData;
};

struct NTP_Time_s {
	int timer;
	int period;

	int initialized;
	int timeSet;

	struct tm now;

	int currentDay;
	int dayChanged;
};

struct Buzzer_s{
  int timer;
  int period;     //Periode entre appels a la fonction
  int pin;
  int value;
  int freq;       //Frenquece du signal en sortie
};

/**
 * task struct declarations
 */
struct Led_s 		Led1;
struct Mess_s 		Mess1;
struct Display_s 	Disp1;
struct Ligth_s 		Light1;
struct Button_s 	Button1;
struct GSM_s    	Gsm1;
struct WIFI_s 		Wifi1;
struct MQTT_s 		Mqtt1;
struct Linky_s 		Linky1;
struct BLE_s		Ble1;
struct NTP_Time_s	Time1;
struct Buzzer_s 	Buzzer1;

struct Boite_s Boite1;
struct Boite_s BoiteLed;
struct Boite_s BoiteBtn_Bzz;

struct Boite_s com_alarmLight;
struct Boite_s com_buttonToDisplay;

/**
 * @brief waitFor is a non blocking function t_sleep_and_displaythat returns the number periods
 * passed science the last function call for the specified timer. 
 * 
 * @param timer  No. timer to use
 * @param period  Period to thest
 * @return int Number of periods elapsed since last call
 */
int waitFor(int timer, unsigned long period){
	if (!period) {
		Serial.println("waitFor ERROR, period must be greter than zero");
		return 0;
	}
	
	static unsigned long waitForTimer[MAX_WAIT_FOR_TIMER];  // il y a autant de timers que de tâches périodiques
	unsigned long newTime = micros() / period;              // numéro de la période modulo 2^32 
	int delta = newTime - waitForTimer[timer];              // delta entre la période courante et celle enregistrée

	if ( delta < 0 )
		delta = 1 + newTime;                   // en cas de dépassement du nombre de périodes possibles sur 2^32 
	if ( delta )
		waitForTimer[timer] = newTime;             // enregistrement du nouveau numéro de période

	return delta;
}

/**
 * @brief Enter ESP32 in deep_sleep mode
 * If no wake up method was configured, ESP32 will sleep
 * permanently
 * 
 */
void deep_sleep()
{
	Serial.println("ESP32 getting into deep sleep mode");
	Serial.flush();
	esp_sleep_enable_timer_wakeup(SLEEP_TIME);
	esp_deep_sleep_start();
}

/**
 * @brief Enter ESP32 in deep_sleep mode
 * If no wake up method was configured, ESP32 will sleep
 * permanently
 * 
 */
void light_sleep()
{
	Serial.println("ESP32 getting into light sleep mode");
	Serial.flush();
	esp_sleep_enable_timer_wakeup(SLEEP_TIME);
	esp_light_sleep_start();
}

int stateSimulateTic = 0;

void write_TIC_label(char *label, char *info)
{
		linkySerialGlobal.print(static_cast<char>(0x0A));
		linkySerialGlobal.print(label);
		linkySerialGlobal.print(static_cast<char>(0x09));
		linkySerialGlobal.print(info);
		linkySerialGlobal.print(static_cast<char>(0x09));
		linkySerialGlobal.print('X');
		linkySerialGlobal.print(static_cast<char>(0x0D));
		linkySerialGlobal.flush();
}

int dataI = 8;
void simulate_TIC()
{
	if (!(waitFor(13, 16700)))
		return;

	//int dataI = random(0, 30);

	String ppap(dataI * 220, DEC);
	int len = ppap.length();
	if (len == 1) {
		ppap = "0000" + ppap;
	} else if (len == 2) {
		ppap = "000" + ppap;
	} else if (len == 3) {
		ppap = "00" + ppap;
	} else if (len == 4) {
		ppap = "0" + ppap;
	}

	switch (stateSimulateTic)
	{
	case 0:
		linkySerialGlobal.print(static_cast<char>(STX));
		write_TIC_label("ADCO", "022061021363");
		stateSimulateTic++;
	case 1:
		write_TIC_label("OPTARIF", "HC..");
		stateSimulateTic++;
	case 2:
		write_TIC_label("ISOUSC", "45");
		stateSimulateTic++;
	case 3:
		write_TIC_label("HCHC", "005390280");
		stateSimulateTic++;
	case 4:
		write_TIC_label("HCHP", "007914071");
		stateSimulateTic++;
	case 5:
		write_TIC_label("PTEC", "HP..");
		stateSimulateTic++;
	case 6:
		write_TIC_label("IINST", (char*)String(dataI, DEC).c_str());
		stateSimulateTic++;
	case 7:
		write_TIC_label("IMAX", "090");
		stateSimulateTic++;
	case 8:
		write_TIC_label("PPAP", (char*)ppap.c_str()); //"03440"
		stateSimulateTic++;
	case 9:
		write_TIC_label("HHPHC", "A");
		stateSimulateTic++;
	case 10:
		write_TIC_label("MOTDETAT", "000000");
		linkySerialGlobal.print(static_cast<char>(ETX));
		linkySerialGlobal.flush();
		stateSimulateTic++;
	case 11:
		stateSimulateTic++;
		break;
	case 12:
		stateSimulateTic = 0;
		break;
	default:
		stateSimulateTic = 0;
		break;
	}

	return;
}

/**
 * @brief This functions handles commands recived from diferents sources.
 * the response will be sent depending on the source (if there is any response)
 * Sources:
 * 		- CMD_SRC_SERIAL
 * 		- CMD_SRC_SMS
 * 		- CMD_SRC_MQTT
 * 		- CMD_SRC_BLE
 * 
 * Syntax of a command
 * 	
 * 	"CMD"<space><COMMAND><space>"-v"<value1><space>"-v"<space><value2><space>"-v"<space><value3> .....
 * 
 * 	CMD DEEP_SLEEP
 * 	CMD LIGHT_SLEEP
 * 	CMD SEND_SMS -v Test sms
 * 	CMD SEND_SMS_TO -v +330749187590 -v test sms
 * 	
 * 
 * @param data 
 * @param source 
 */
void text_command(String &data, int source)
{	
	
	String command;
	String value[NB_MAX_COMMANDS];
	String dest_number;
	int nbValues = 0; //Test if number of values match the command
	int i;
	String retData;

	if (data.length() == 0)
		return;

	/**
	 * @brief This seccion parse the comand recived in the parameter data
	 * The parameter, since it's passes by reference, the value in the origin
	 * will be modified, so the use of a temporary variable is recommended
	 */
	
	if (data.startsWith("CMD")) {
		data.remove(0, 4); // Remove "CMD"<space>
		
		//Get command
		if (data.indexOf("-v") != -1) {
			command = data.substring(0, data.indexOf("-v") - 1); //get COMMAND
			data.remove(0, data.indexOf("-v")); // remove "COMMAND"<space>
		} else {
			command = data;
		}

		for (i = 0; i < NB_MAX_COMMANDS; i++) {
			if (data.indexOf("-v") != -1) { // Is there a value?
				data.remove(0, 3); //remove "-v"<space>
				
				if (data.indexOf("-v") != -1) { //is Htere another value?
					value[i] = data.substring(0, data.indexOf("-v") - 1);
					data.remove(0, data.indexOf("-v"));
				} else { // Lasy value
					value[i] = data;
					break;
				}
			} else {
				break;
			}
		}

		nbValues = i + 1;
		
	} else {
		retData = "Invalid commmand syntax\n";
		retData += "\"CMD<space><COMMAND><space>\"-v\"<value1><space>\"-v\"<space><value2><space>\" ...";
		goto txt_cmd_sendResponse;
	}


	/**
	 * @brief This section handles the command recived and
	 * executes the instructions. The parse step does not check 
	 * if the command exists or the number of values are corrects
	 */


	if (command.startsWith("DEEP SLEEP")) {
		deep_sleep();
		return;
	} 
	
	if (command.startsWith("LIGHT SLEEP")) {
		light_sleep();
		return;
	} 

	if (command.startsWith("SEND SMS TO")) {
		if (nbValues < 2) {
			retData = "Incorrect number of values. Must be -v <DESTINATION NUMBER> -v <SMS DATA>";
			goto txt_cmd_sendResponse;
		} else {
			SMS.send(value[0], value[1]);
			return;
		}
	}
	
	if (command.startsWith("SEND SMS")) {
		SMS.send(number, value[0]);
		return;
	}
	
	if (command.startsWith("DELETE ALL SMS")) {
		int nbSmsDel = SMS.delete_all_SMS();
		return;
	}
	
	if (command.startsWith("STATUS")) {
		retData = noWattStatus();
		goto txt_cmd_sendResponse;
	}

	if (command.startsWith("LIGHT LEVEL")) {
		retData  = "Raw value: ";
		retData += Light1.val;
		goto txt_cmd_sendResponse;
	}

	if (command.startsWith("BOOT COUNT")) {
		retData  = "Boot Count: ";
		retData += bootCount;
		goto txt_cmd_sendResponse;
	}

	if (command.startsWith("LINKY MAIN DATA")) {
		retData = "Current: ";
		retData += Linky1.current;
		retData += "\nvoltage: ";
		retData += Linky1.voltage;
		retData += "\npower(S): ";
		retData += Linky1.power;
		goto txt_cmd_sendResponse;
	}

	if (command.startsWith("LINKY STATS DATA")) {
		retData = "Mean I : ";
		retData += Linky1.statsCurrent.stats.average;
		retData += "\nMean V : ";
		retData += Linky1.statsVoltage.stats.average;
		retData += "\nMean VA: ";
		retData += Linky1.statsPower.stats.average;
		goto txt_cmd_sendResponse;
	}

	if (command.startsWith("GET LINKY CURRENT HISTORY")) {
		retData = "CURRENT LINKY AVERAGE MEASUREMENT HISTORIC\n";
		print_historic_data(&Linky1.historicCurrent);
		return;
	}

	if (command.startsWith("GET TIME"))	{
		char tmp[50];
		strftime(tmp, sizeof(tmp), "%A, %B %d %Y %H:%M:%S", &Time1.now);
		retData = String(tmp);
		goto txt_cmd_sendResponse;
	}

	if (command.startsWith("SET CURRENT LIMIT")) {
		Linky1.maxLimit = value[0].toInt();
		return;
	}

	if (command.startsWith("TMP SET CURRENT")) {
		dataI = value[0].toInt();
		return;
	}

	/*If this return is executed, means there is no response to send*/
	retData  = "Available commands:";
	retData += "\nCMD DEEP SLEEP";
	retData += "\nCMD LIGHT SLEEP";
	retData += "\nCMD SEND SMS TO -v <destNum> -v <smsData>";
	retData += "\nCMD SEND SMS -v <smsData>";
	retData += "\nCMD DELETE ALL SMS";
	retData += "\nCMD STATUS";
	retData += "\nCMD LIGHT LEVEL";
	retData += "\nCMD BOOT COUNT";
	retData += "\nCMD LINKY MAIN DATA";
	retData += "\nCMD LINKY STATS DATA";
	retData += "\nCMD GET TIME";

	if (source == CMD_SRC_SERIAL) {
		/*Exclusive commands to SERIAL*/

		retData += "\nCMD GET LINKY CURRENT HISTORY";
	} else if (source == CMD_SRC_SMS) {

	}

	/**
	 * @brief This final section send a answert to the source of 
	 * the command. Not all commands send data.
	 * Acknowledges are not sent
	 */

txt_cmd_sendResponse:

	switch (source)
	{
	case CMD_SRC_SERIAL:
		Serial.println(retData);
		break;
	case CMD_SRC_SMS:
		SMS.send(number, retData);
		break;
	case CMD_SRC_MQTT:
		
		break;
	case CMD_SRC_BLE:
		
		break;
	
	default:
		break;
	}

	return;

}

/**
 * @brief Returns a string with general status of functionalities
 */
String noWattStatus()
{
	String statusSMS("noWatt STATUS---");

	statusSMS += (LINKY.isInitialized())	? "\nLinky: Initialized": "\nLinky: ----";
	statusSMS += (Linky1.connected)		? "\nLinky: Online"		: "\nLinky: ----";
	statusSMS += (Gsm1.isInitialized)	? "\nGSM: Online" 		: "\nGSM: ----";
	statusSMS += (Ble1.initialized) 	? "\nBLE: Online" 		: "\nBLE: ----";
	statusSMS += (Wifi1.connected) 		? "\nWIFI: Online" 		: "\nWIFI: ----";
	statusSMS += (Mqtt1.connected) 		? "\nMQTT: Online" 		: "\nMQTT: ----";
	statusSMS += "\nboot count: " + String(bootCount, DEC);

	return statusSMS;
}

/**
 * @brief Calculates general
 * 
 * @param dataOut 
 * @param dataIn 
 */
void statistics(struct measurement_statistics *stats)
{
	int nbData = stats->nbElements;
	
	int sum = 0;
	int average;

	if (nbData == 0)
		return;

	for (int i = 0; i < nbData; i++) {
		sum += stats->measurements[i].value;

		if (stats->measurements[i].value < stats->stats.min and stats->measurements[i].value != -1)
			stats->stats.min = stats->measurements[i].value;

		if (stats->measurements[i].value > stats->stats.max)
			stats->stats.max = stats->measurements[i].value;
	}

	average = sum / nbData;
	stats->stats.average = average;
	
}

/**
 * @brief This function will be usually after the reception of a timer interrupt.
 * 
 * @param historic 
 */
void store_in_historc(struct measurement_historic *historic, int value)
{	
	/* If time not set, leave space at 0 */
	if (Time1.timeSet) {
		historic->measurements[historic->index].timestamp = Time1.now;
	}

	historic->measurements[historic->index].value = value;

	if (historic->nbElements < LINKY_HISTORY_SIZE)
		historic->nbElements++;

	historic->index++;
	if (historic->index >= LINKY_HISTORY_SIZE)
		historic->index = 0;
}

void print_historic_data(struct measurement_historic *historic)
{
	String data;
	char tmp[50];
	/**
	 * @brief If historic is not full, iterations start from the begining of the array (0)
	 * if it is full (test is false), iteration start from index + 1, wich means, oldest to lastest
	 * elemnts
	 */
	int index = (historic->index == (historic->nbElements)) ? 0 : historic->index + 1;
	for (int i = 0; i < historic->nbElements; i++) {
		/*Timestamp*/
		data += i;
		data += ": ";
		strftime(tmp, sizeof(tmp), "%A, %B %d %Y %H:%M:%S", &historic->measurements[index].timestamp);
		data += String(tmp);
		/*Value*/
		data += "\t";
		data += historic->measurements[index].value;
		data += "\n";

		index++;
		if (index >= LINKY_HISTORY_SIZE)
			index = 0;
	}

	Serial.println(data);

}

/**
 * @brief Executed periodicaly. This functions periodically check structre variables
 * of type Booite_s qui containing a flag and data space. Generally, if flag is one,
 * it means that the alarm is setted
 */
void alarms()
{
	if(!(waitFor(16, 1 * 1000000))) // Every 1 second
		return;

	globalAlarms = "";

	/**
	 * @brief LIGHT INTENCITY ALARM
	 */
	if (com_alarmLight.flag) {
		String alarm("Light too High:");
		alarm += com_alarmLight.nextNotification;
		globalAlarms += alarm + "||";
		
		/**
		 * @brief Notification will be sent only oonce every ~10 minutes.
		 * 
		 */
		if (com_alarmLight.nextNotification == 0) {
			Serial.println("SENDING ALARM SMS");
			//SMS.send(number, alarm);

			com_alarmLight.nextNotification = 60 * 10; // every 10 minutes
		}
	}

	if (Linky1.statsCurrent.stats.average > Linky1.maxLimit && Linky1.maxLimit != -1) {
		String alarm("Current too hight:");
		alarm += Linky1.statsCurrent.stats.average;
		globalAlarms += alarm + "||";

		if (Linky1.maxLimitAlarmOnceDay == 0) {
			Serial.println("SENDING ALARM SMS");
			SMS.send(number, alarm);

			Linky1.maxLimitAlarmOnceDay = 1;
		}
	}
	
	if (com_alarmLight.nextNotification > 0)
		com_alarmLight.nextNotification--;

}

/**
 * @brief There are some alarms that only are sent once a day
 * This function is intended to reset daily information 
 * Even calling daily routins is a good pratice in this function
 */
void update_new_day_info()
{
	Linky1.maxLimitAlarmOnceDay = 0;
}


unsigned long previousTime = 0;
int shortP = 0;
int longP = 0;

void short_press()
{
	Serial.println("Short Press");
	com_buttonToDisplay.flag  = FULL;
	com_buttonToDisplay.bData = 1;
}

void long_press()
{
	Serial.println("Long Press");
	com_buttonToDisplay.flag  = FULL;
	com_buttonToDisplay.bData = 2;
}

int touchPressType()
{	
	unsigned long current_time = millis();
	unsigned long elapsed_time = current_time - previousTime;
	if (get_touch_boolean()) {
		if (elapsed_time < SHORT_TOUCH_MAX_MS and elapsed_time > SHORT_TOUCH_MIN_MS) {
			if (!shortP) {
				//short_press();
				shortP = 1;
			}
			return 1;
		} else if (elapsed_time >= SHORT_TOUCH_MAX_MS) {
			if (!longP) {
				long_press();
				longP = 1;
			}
			return 2;
		} 
	} else {
		previousTime = current_time;
		if (shortP == 1 and longP == 0) {
			short_press();
		}
		shortP = 0;
		longP = 0;
	}

	return 0;
}

int get_touch_boolean()
{
	//Serial.println(touchRead(TOUCH_PIN_0));
	return (touchRead(TOUCH_PIN_0) < 35) ? 1 : 0;
}

int get_touch_value()
{
	touchRead(TOUCH_PIN_0);
}

/*********************************************************/
/* Definition of setup and loop task  */
/*********************************************************/

void setup_Led( struct Led_s * ctx, int timer, unsigned long period, byte pin) {
	ctx->timer = timer;
	ctx->period = period;
	ctx->pin = pin;
	ctx->etat = 1;
	ctx->duty = 0;

	/**
	 * Configuration du canal PWM pour la led. 
	 */
	ledcAttachPin(pin, PWM_LEDCHANNEL);
	ledcSetup(PWM_LEDCHANNEL, PWM_FREQ_BASE, PWM_RESOLUTION);
}

void loop_Led( struct Led_s * ctx, struct Boite_s *boiteLed) {
	
	if(!(waitFor(ctx->timer,ctx->period)))
		return;

	float tmpDutyCycle; 
	
	if(boiteLed->flag ==  1){ //Si flag = 1, Message pendants
		boiteLed->flag = 0;
		ctx->duty = boiteLed->bData;  //Lécture et stockage du message

		/**
		 * Tester que duty cycle != de zero sino divition par zero. Exeption
		 * Calculer le rapport cyquile de 0 - 1 avec point décimal
		 */
		if(ctx->duty != 0)tmpDutyCycle = (float(ctx->duty) / (float)100);
		else tmpDutyCycle;

		/**
		 * 0 -> 0% ..... 128 -> 50% ....... 255 -> 100%
		 */
		ledcWrite(PWM_LEDCHANNEL, 255*tmpDutyCycle); 
	}                
}

//--------- definition de la tache Mess

/*********************************************************/
/* Deffinition of task Serial message  */
/*********************************************************/

/**
 * Message affiches des information sur la ligne de commande
 */
void setup_Mess( struct Mess_s * ctx, int timer, unsigned long period, const char * mess) {
	ctx->timer = timer;
	ctx->period = period;
	
	strcpy(ctx->mess, mess);
	Serial.begin(9600);     // initialisation du débit de la liaison série
}

void loop_Mess(struct Mess_s *ctx, struct Led_s *led, struct Boite_s *boite) {
	
	if (!(waitFor(ctx->timer,ctx->period)))
		return;         // sort s'il y a moins d'une période écoulée
		
	return;
	Serial.print(ctx->mess); // affichage du message
	Serial.print(" PWM Level: ");
	Serial.print(led->duty);
	Serial.print(" Light leve: ");
	Serial.println(boite->bData);
}

/*********************************************************/
/* Definition de la tache light sensor  */
/*********************************************************/

void setup_Light(struct Ligth_s *ctx, int timer, int pin, int period){
	ctx->timer = timer;
	ctx->period = period;
	ctx->pin = pin;

	//Alarm data initialization
	com_alarmLight.nextNotification = 0;
}

void loop_Light(struct Ligth_s *ctx, struct Boite_s *boite, struct Boite_s *boiteLed){
	/**
	 * On envoi des valuer par boite aux lettre. On ne teste pas si la valuer précédente a été lue, 
	 * On n'as pas besoir d'aquiter la reception d'une donné
	 */
	if ((waitFor(ctx->timer,ctx->period))){ 
		ctx->val = analogRead(ctx->pin);

		//Convertir la valeur de 0-4096 à 0% - 100%
		unsigned int valTmp = (unsigned int)(((float)(ANALOG_MAXVAL - ctx->val) / (float)ANALOG_MAXVAL) * 100);
		boite->bData = valTmp;

		if (valTmp > 90) {
			com_alarmLight.flag = 1;
		} else {
			com_alarmLight.flag = 0;
		}
	}
	
	if(boiteLed->flag == 0){  //Si != de 0. Les valeurs précedences n'ont pas encore été traités
		if ((waitFor(ctx->timer+1,ctx->period*5))){ 
			boiteLed->flag = 1; //Le flag indique à la tâche lecteur qu'il y a un donnée READY

			unsigned int valTmp = (unsigned int)(((float)(ANALOG_MAXVAL - ctx->val) / (float)ANALOG_MAXVAL) * 100);
			boiteLed->bData = valTmp;
			if(boiteLed->bData == 0) boiteLed->bData = 1; //Eviter que PWM égal a 0
		}
	}
}

/*********************************************************/
/* Display functions definitions */
/*********************************************************/

void setup_Display(struct Display_s * ctx, int timer, int period){
	ctx->timer 				= timer;
	ctx->period 			= period;
	ctx->stringData 		= "String Test";
	ctx->aliveStr			= "-";
	ctx->curScreen 			= 0;
	ctx->optionScreen 		= 0;
	ctx->mainReturnCount 	= 0;
	
	Wire.begin(PIN_SDA, PIN_SCL); // Initialisation communication I2C
	if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
		Serial.println(F("SSD1306 allocation failed"));
	}
	
	display.display();
	delay(500); // Show adafruit logo for 500ms
	display.clearDisplay();
}

void loop_Display(struct Display_s *ctx){
	
	int periods = (waitFor(ctx->timer,ctx->period));
	if (!periods)
		return;   // return si la periode n'est pas encore passé

loop_display_restart:

	/* Detect button press type to change screen or execute option */
	/* Options are handled by each screen function */
	if (com_buttonToDisplay.flag == FULL) {
		if (com_buttonToDisplay.bData == 1) {
			Disp1.curScreen++;
			if (Disp1.optionScreen)
				Disp1.optionScreen = 0;
		} else if (com_buttonToDisplay.bData == 2) {
			Disp1.optionScreen++;
		}

		com_buttonToDisplay.flag = EMPTY;
		com_buttonToDisplay.bData = 0;
	}

	if (Disp1.curScreen >= NB_SCREENS)
		Disp1.curScreen = 0;

	if (Disp1.curScreen != 0) {
		ctx->mainReturnCount += periods;
		if (ctx->mainReturnCount > 120 * 4) { //Period is 250ms * 120 * 4 = 120s
			ctx->mainReturnCount = 0;
			//ctx->curScreen = 0;
		}
	} else {
		ctx->mainReturnCount = 0;
	}

	switch (ctx->curScreen)
	{
	case 0:
		displayScreenMain(ctx);
		break;
	case 1:
		displayGeneralScreen(ctx);
		break;
	case 2:
		/* if (Linky1.connected == 0)
			goto loop_display_screenNotAvail; */

		if (LINKY.isInitialized() == 0)
			goto loop_display_screenNotAvail;

		displayLinkyScreen(ctx);
		break;
	case 3:
		if (Gsm1.isInitialized == 0)
			goto loop_display_screenNotAvail;

		displayGsmScreen(ctx);
		break;
	case 4:
		if (Ble1.initialized == 0)
			goto loop_display_screenNotAvail;

		displayBLEScreen(ctx);
		break;
	case 5:
		if (Mqtt1.connected == 0)
			goto loop_display_screenNotAvail;

		displayMqttScren(ctx);
		break;
	case 6:
		displayLightScreen(ctx);
		break;
	case 7:
		displayAlarmsScreen(ctx);
		break;
	case 8:
		displayAboutScreen(ctx);
		break;
	default:
		break;
	}

	ctx->aliveStr = (ctx->aliveStr.indexOf('-') != -1) ? " " : "-";

	return; 

loop_display_screenNotAvail:
	ctx->curScreen++;
	goto loop_display_restart;
}

void displayScreenMain(struct Display_s *ctx)
{
	/**
	 * Construction du string à affciher.
	 * Tout le string est stocké dans l'attribut du Display.
	 * 
	 * la fonction appendLine permet de remplir une linge avec de SPACE pour 
	 * avoir l'inforrmation suivant dans la ligne suivante.
	 */

	if (ctx->optionScreen)
		ctx->optionScreen = 0; 

	ctx->stringData = ""; // Clear previous data

	ctx->stringData += "noWatt Main";

	ctx->stringData += "       ";
	ctx->stringData += ctx->aliveStr;

	appendLine(ctx->stringData);
	
	if (isTimeSet == 1) {
		switch (Time1.now.tm_wday) //week day
		{
			case 0: ctx->stringData += "Sunday"; 	break;
			case 1: ctx->stringData += "Monday"; 	break;
			case 2: ctx->stringData += "Tuesday"; 	break;
			case 3: ctx->stringData += "Wednesday"; break;
			case 4: ctx->stringData += "Thursday"; 	break;
			case 5: ctx->stringData += "Friday"; 	break;
			case 6: ctx->stringData += "Saturday"; 	break;
			default: ctx->stringData += "---------"; break;
		}

		ctx->stringData += " ";
		ctx->stringData += Time1.now.tm_hour;
		ctx->stringData += ":";
		ctx->stringData += Time1.now.tm_min;
		ctx->stringData += ":";
		ctx->stringData += Time1.now.tm_sec;
	} else {
		ctx->stringData += "No Time Connect WIFI";
	}

	appendLine(ctx->stringData);

	//BUZZER STATUS
	ctx->stringData += "LinkyS:";
	ctx->stringData += (LINKY.isInitialized() == 1) ? "Init" : "----";
	ctx->stringData += (Linky1.connected == 1)   ? " Online" : " ------";

	appendLine(ctx->stringData);

	//BUZZER STATUS
	ctx->stringData += "GMS :";
	ctx->stringData += (Gsm1.isInitialized == 1) ? "Connected" : "---------";

	appendLine(ctx->stringData);

	//BUZZER STATUS
	ctx->stringData += "BLE :";
	ctx->stringData += (Ble1.initialized == 1) ? "initialized" : "---------";

	appendLine(ctx->stringData);

	//BUZZER STATUS
	ctx->stringData += "WIFI:";
	ctx->stringData += (Wifi1.connected == 1) ? "Connected" : "---------";

	appendLine(ctx->stringData);

	//BUZZER STATUS
	ctx->stringData += "MQTT:";
	ctx->stringData += (Mqtt1.connected == 1) ? "Connected" : "---------";

	appendLine(ctx->stringData);

	//BUZZER STATUS
	ctx->stringData += "Boot Count:";
	ctx->stringData += bootCount;
	
	testdrawtext((char*)ctx->stringData.c_str());
	
}

void displayGeneralScreen(struct Display_s *ctx)
{

	if (ctx->optionScreen)
		ctx->optionScreen = 0;


	/**
	 * Transforme la valeur analogic en pourcentage 0 - 100%
	 */
	unsigned int rawToPrc = (unsigned int)(((float)(ANALOG_MAXVAL - Light1.val) / (float)ANALOG_MAXVAL) * 100);

	/**
	 * Construction du string à affciher.
	 * Tout le string est stocké dans l'attribut du Display.
	 * 
	 * la fonction appendLine permet de remplir une linge avec de SPACE pour 
	 * avoir l'inforrmation suivant dans la ligne suivante.
	 */

	ctx->stringData = ""; // Clear previous data

	ctx->stringData += "noWatt GenericData";

	ctx->stringData += "";
	ctx->stringData += ctx->aliveStr;
	ctx->aliveStr = (ctx->aliveStr.indexOf('-') != -1)? " " : "-";

	appendLine(ctx->stringData);

	//BUZZER STATUS
	ctx->stringData += "GMS";
	ctx->stringData += (Gsm1.isInitialized == 1) ? "*" : "-";

	//BUZZER STATUS
	ctx->stringData += " WIFI";
	ctx->stringData += (Wifi1.connected == 1) ? "*" : "-";

	//BUZZER STATUS
	ctx->stringData += " MQTT";
	ctx->stringData += (Mqtt1.connected == 1) ? "*" : "-";

	appendLine(ctx->stringData);

	//BUZZER STATUS
	ctx->stringData += "Boot:";
	ctx->stringData += bootCount;

	//Light LEVEL
	ctx->stringData += " Light:";
	ctx->stringData += String(rawToPrc, DEC);
	ctx->stringData += "%";

	appendLine(ctx->stringData);

	//BTN VALUE
	ctx->stringData += "ButtonVl: ";
	ctx->stringData += Button1.value;

	appendLine(ctx->stringData);

	//BUZZER STATUS
	ctx->stringData += Gsm1.sender;

	appendLine(ctx->stringData);

	ctx->stringData += Gsm1.data;
	
	int stringlen = ctx->stringData.length() + 1; //Obtenir la longeur du string
	
	/**
	 * Transfomrer le chaine de type string en char*. 
	 */
	char tmp[stringlen];                          
	ctx->stringData.toCharArray(tmp, stringlen); // Cast string to char*
	tmp[stringlen] = 0;
	
	testdrawtext(tmp);
}

void displayLinkyScreen(struct Display_s *ctx)
{
	
	ctx->stringData = ""; // Clear previous data

	if (ctx->optionScreen > 1)
		ctx->optionScreen = 0;

	if (ctx->optionScreen == 0) {

		ctx->stringData += "noWatt - Linky";
		
		ctx->stringData += "    ";
		ctx->stringData += ctx->aliveStr;

		appendLine(ctx->stringData);
		appendLine(ctx->stringData);

		//BTN VALUE
		ctx->stringData += "New data indicator ";
		ctx->stringData += (Linky1.newData) ? "*" : "-";

		appendLine(ctx->stringData);

		//BTN VALUE
		ctx->stringData += "Current :";
		ctx->stringData += Linky1.current;
		ctx->stringData += " A";

		appendLine(ctx->stringData);

		ctx->stringData += "Voltage :";
		ctx->stringData += Linky1.voltage;
		ctx->stringData += " V";

		appendLine(ctx->stringData);

		ctx->stringData += "Power(S):";
		ctx->stringData += Linky1.power;
		ctx->stringData += " VA";

		appendLine(ctx->stringData);	
	} else if (ctx->optionScreen == 1){
		ctx->stringData += "noWatt - Linky";
		
		ctx->stringData += "    ";
		ctx->stringData += ctx->aliveStr;

		appendLine(ctx->stringData);

		//BTN VALUE
		ctx->stringData += "Statistic Data  ";
		ctx->stringData += (Linky1.newData) ? "*" : "-";

		appendLine(ctx->stringData);
		appendLine(ctx->stringData);

		ctx->stringData += "V:";
		ctx->stringData += Linky1.statsVoltage.stats.min;
		ctx->stringData += "  ";
		ctx->stringData += Linky1.statsVoltage.stats.average;
		ctx->stringData += "  ";
		ctx->stringData += Linky1.statsVoltage.stats.max;

		appendLine(ctx->stringData);

		ctx->stringData += "I:";
		ctx->stringData += Linky1.statsCurrent.stats.min;
		ctx->stringData += "  ";
		ctx->stringData += Linky1.statsCurrent.stats.average;
		ctx->stringData += "  ";
		ctx->stringData += Linky1.statsCurrent.stats.max;

		appendLine(ctx->stringData);

		ctx->stringData += "P:";
		ctx->stringData += Linky1.statsPower.stats.min;
		ctx->stringData += "  ";
		ctx->stringData += Linky1.statsPower.stats.average;
		ctx->stringData += "  ";
		ctx->stringData += Linky1.statsPower.stats.max;

		appendLine(ctx->stringData);
		appendLine(ctx->stringData);

		ctx->stringData += "I limit: ";
		ctx->stringData += Linky1.maxLimit;

	}

	testdrawtext((char*)ctx->stringData.c_str());
}

void displayGsmScreen(struct Display_s *ctx)
{
	
	if (ctx->optionScreen) {
		String statusTmp = noWattStatus();
		SMS.send(number, statusTmp);
		Disp1.optionScreen = 0;

		/* 		String statusSMS("noWatt STATUS---");

		statusSMS += (LINKY.isInitialized()) 	? "\nLinky: Initialized"	: "\nLinky: ----";
		statusSMS += (false) 				? "\nLinky: Online"		: "\nLinky: ----";
		statusSMS += (Gsm1.isInitialized) 	? "\nGSM: Online" 		: "\nGSM: ----";
		statusSMS += (Wifi1.connected) 		? "\nWIFI: Online" 		: "\nWIFI: ----";
		statusSMS += (Mqtt1.connected) 		? "\nMQTT: Online" 		: "\nMQTT: ----";
		statusSMS += "\nboot count: " + String(bootCount, DEC); */
	}
	
	ctx->stringData = ""; // Clear previous data

	ctx->stringData += "noWatt - GSM";

	ctx->stringData += "      ";
	ctx->stringData += ctx->aliveStr;

	appendLine(ctx->stringData);
	appendLine(ctx->stringData);

	//BUZZER STATUS
	ctx->stringData += Gsm1.sender;

	appendLine(ctx->stringData);
	appendLine(ctx->stringData);

	ctx->stringData += Gsm1.data;

	testdrawtext((char*)ctx->stringData.c_str());
}

void displayMqttScren(struct Display_s *ctx)
{

	if (ctx->optionScreen)
		ctx->optionScreen = 0;

	ctx->stringData = ""; // Clear previous data

	ctx->stringData += "noWatt - MQTT";

	ctx->stringData += "     ";
	ctx->stringData += ctx->aliveStr;

	appendLine(ctx->stringData);
	appendLine(ctx->stringData);

	testdrawtext((char*)ctx->stringData.c_str());
}

void displayBLEScreen(struct Display_s *ctx)
{

	if (ctx->optionScreen)
		ctx->optionScreen = 0;

	ctx->stringData = ""; // Clear previous data

	ctx->stringData += "noWatt - BLE";

	ctx->stringData += "      ";
	ctx->stringData += ctx->aliveStr;

	appendLine(ctx->stringData);
	appendLine(ctx->stringData);

	ctx->stringData += "DevName:";
	ctx->stringData += Ble1.deviceName;

	appendLine(ctx->stringData);

	ctx->stringData += "Data:";
	ctx->stringData += Ble1.manufacturerData;

	testdrawtext((char*)ctx->stringData.c_str());
}

void displayLightScreen(struct Display_s *ctx)
{

	if (ctx->optionScreen)
		ctx->optionScreen = 0;

	/**
	 * Transforme la valeur analogic en pourcentage 0 - 100%
	 */
	unsigned int rawToPrc = (unsigned int)(((float)(ANALOG_MAXVAL - Light1.val) / (float)ANALOG_MAXVAL) * 100);

	ctx->stringData = ""; // Clear previous data

	ctx->stringData += "noWatt - Light S";

	ctx->stringData += "  ";
	ctx->stringData += ctx->aliveStr;

	appendLine(ctx->stringData);
	appendLine(ctx->stringData);

	//Light LEVEL
	ctx->stringData += "Light:";
	ctx->stringData += String(rawToPrc, DEC);
	ctx->stringData += "%";

	appendLine(ctx->stringData);
	appendLine(ctx->stringData);
	appendLine(ctx->stringData);

	ctx->stringData += "0%              100%";

	
	
	testdrawtext((char*)ctx->stringData.c_str());

	//Draw a line
	int y = 50; 
	for (int i = 0; i < (int)((float)rawToPrc * 1.28); i++) {
		display.drawPixel(i,y-1, SSD1306_WHITE);
		display.drawPixel(i,y, SSD1306_WHITE);
		display.drawPixel(i,y+1, SSD1306_WHITE);
	}
	display.display();
}

void displayAlarmsScreen(struct Display_s *ctx)
{
	if (ctx->optionScreen)
		ctx->optionScreen = 0;

	ctx->stringData = ""; // Clear previous data

	ctx->stringData += "ALARMS";

	ctx->stringData += "           ";
	ctx->stringData += ctx->aliveStr;

	appendLine(ctx->stringData);
	appendLine(ctx->stringData);

	ctx->stringData += globalAlarms;

	testdrawtext((char*)ctx->stringData.c_str());
}

void displayAboutScreen(struct Display_s *ctx)
{

	if (ctx->optionScreen)
		ctx->optionScreen = 0;

	ctx->stringData = ""; // Clear previous data

	ctx->stringData += "ABOUT";

	ctx->stringData += "            ";
	ctx->stringData += ctx->aliveStr;

	appendLine(ctx->stringData);

	ctx->stringData += "noWatt project";
	appendLine(ctx->stringData);

	ctx->stringData += "Version: ";
	ctx->stringData += CURRENT_VERSION;
	appendLine(ctx->stringData);

	ctx->stringData += "Author:Martin ACOSTA";
	appendLine(ctx->stringData);

	ctx->stringData += "LIP6";
	appendLine(ctx->stringData);

	ctx->stringData += "Frank WAJSBURT";
	appendLine(ctx->stringData);

	testdrawtext((char*)ctx->stringData.c_str());
}

void testdrawtext(char* text) {
	//21 characters par ligne
	display.clearDisplay();
	display.setTextSize(0);      // Normal 1:1 pixel scale
	display.setTextColor(SSD1306_WHITE); // Draw white text
	
	display.setCursor(0, 0);     // Start at top-left corner
	display.cp437(true);         // Use full 256 char 'Code Page 437' font
	display.write(text);
	display.display();
}

// Concatenate SPACE to pass to next line
void appendLine(String &str){
	for(int i = (str.length() % 21); i < 21 ; i++){
		str += ' ';
	}
}

/*********************************************************/
/* Button Functions definitions */
/*********************************************************/

/**
 * Handler du interruption boutton
 */
/* void IRAM_ATTR isr() {
	Serial.println("BUTTON INTERRUPTION RECIVED");
} */

void setup_Button(struct Button_s *button, int timer, int period, int pin){
	button->timer	= timer;
	button->period	= period;
	button->pin		= pin;
	button->value	= 0; //Init value a zero

	button->pressCount	= 0;
	button->shortPress	= 0;
	button->longPress	= 0;
	button->mustRelease = 0;
	
	pinMode(button->pin, INPUT_PULLUP); //l'option INPUT_PULLUP est nécessaire à cause du mode de connection du boutton

	/**
	 * Déclaration d'une interruption pour le GPIO boutton. 
	 * la fonction appelé sera isr() et FALLING indique le déclanchement
	 * de l'interruption au front décendante du signal
	 */
	//attachInterrupt(button->pin, isr, FALLING); 
}

void publishButtonVal(){
	String tempString;
	int tempLength;
	
	/*Get Button Value and publish it*/
	tempString = String(Button1.value, DEC);

	// Publish buttonValue sur topic = esp32/ButtonState
	//mqtt_client.publish("esp32/ButtonState", tempString.c_str());
}

void loop_Button(struct Button_s *button, struct Boite_s *boite){
	//if (boite->flag == 1) return; //Valeur pas encore lu
	int periodes = (waitFor(button->timer,	button->period)); 
	if (!periodes)
		return;

	/* Read digital input */
	button->value 	= 1 - digitalRead(button->pin);

	/* detect button value change */
	if(button->lastVal != button->value){

		button->lastVal = button->value;

		/* Detect if there were a short or long press and notify display*/ 
		if (button->value == 0) {
			if (button->longPress)
				return;

			button->shortPress = 1;

			com_buttonToDisplay.flag  = FULL;
			com_buttonToDisplay.bData = button->shortPress;
			return;
		}

	}

	/* Detect a long press and notify display via a comm struct */
	if (button->value == 1) {
		button->pressCount += periodes;

		if (button->pressCount > 150) {
			if (button->mustRelease == 0) {
				button->longPress 	= 2;
				button->mustRelease = 1;

				com_buttonToDisplay.flag  = FULL;
				com_buttonToDisplay.bData = button->longPress;
			}
		}
	} else {
		button->pressCount	= 0;
		button->shortPress	= 0;
		button->longPress	= 0;
		button->mustRelease	= 0;
	}
}

/*********************************************************/
/* WIFI functions definitions */
/*********************************************************/

void setup_wifi(struct WIFI_s *ctx, int timer, int period, const char *ssid_param, const char *pass_param) {
	
	ctx->timer 			= timer;
	ctx->period 		= period;
	ctx->initialized 	= 0;
	ctx->connected 		= 0;
	ctx->ssid 			= (char *)ssid_param;
	ctx->pass 			= (char *)pass_param;

	/*We start by connecting to a WiFi network*/
	Serial.print("Initializing WIFI, connecting to: ");
	Serial.println(ctx->ssid);

	// delete old config
	WiFi.disconnect(true);

	// Examples of different ways to register wifi events
	WiFi.onEvent(WiFiEvent);
	//WiFi.onEvent(WiFiGotIP, WiFiEvent_t::SYSTEM_EVENT_STA_GOT_IP);
	
/*   WiFiEventId_t eventID = WiFi.onEvent([](WiFiEvent_t event, WiFiEventInfo_t info){
		Serial.print("WiFi lost connection. Reason: ");
		Serial.println(info.disconnected.reason);
	}, WiFiEvent_t::SYSTEM_EVENT_STA_DISCONNECTED); */
	
	// Station Mode : ESP32 connects to a acces point
	WiFi.mode(WIFI_STA);

	//Begin connection
	WiFi.begin(ssid, password);

	/**
	 * @brief wifi handler will let us know wheter a connection is revied
	 * or a current connection is lost
	 * 
	 */
}

/*********************************************************/
/* MQTT methods */
/*********************************************************/

void reconnect_mqtt()
{
	/*Connection to MQTT broker*/
	
	// Attempt to connect
	if (mqtt_client.connect("ESP8266Client")) {
		Serial.println("connected");
		
		/*Subscribe topics. ESP32 will listen to these topics*/
		mqtt_client.subscribe("esp32/buzzerFreq");
		mqtt_client.subscribe("esp32/buzzerValue");
	}
}

void setup_mqtt(struct MQTT_s *ctx, int timer, int period, const char *server, int port)
{
	ctx->timer 		= timer;  
	ctx->period 	= period;
	ctx->server 	= (char *)server;
	ctx->port 		= port;
	ctx->connected 	= 0;

	mqtt_client.setServer(ctx->server, ctx->port);
	mqtt_client.setCallback(callback);
}

void loop_mqtt(struct MQTT_s *ctx)
{
	/*This function must be called periodically to check the
	server state and call hanlers in case of any server activity*/
	if (!Wifi1.connected)
		return;
	
	if (ctx->connected == 1)
		mqtt_client.loop();

	if(!(waitFor(ctx->timer, ctx->period))) 
		return;
	
	/*Try to connect to mqtt if disconnected and wifi initialized*/
	if (!mqtt_client.connected() and Wifi1.connected == 1) {
		ctx->connected = 0;
		reconnect_mqtt();
		return; 
	} else if (mqtt_client.connected() and Wifi1.connected == 1) {
		ctx->connected = 1;
	} else {
		ctx->connected = 0;
		return;
	}
	
	/*
	 * Publisher will publish all the information showed  
	 * on the esp32 screen. Light Level, PWM Level, Button state
	 * Buzzer Freq et Buzzer State
	 */
	char tempChar[8];
	String tempString;
	int tempLength;
	

	/*Get Light Level and publish*/
	tempString = String(Light1.val,DEC);
	tempLength = tempString.length();
	tempLength += 1; /*add null terminated char*/
	tempString.toCharArray(tempChar, tempLength);
	mqtt_client.publish("esp32/LightLevel", tempChar);

	/*Get PWM Level and publish*/
	tempString = String(Led1.duty,DEC);
	tempLength = tempString.length();
	tempLength += 1; /*add null terminated char*/
	tempString.toCharArray(tempChar, tempLength);
	mqtt_client.publish("esp32/PWMLevel", tempChar);
}


/*********************************************************/
/* Setup and loop of GSM task */
/*********************************************************/

void setup_GSM(struct GSM_s *ctx, int timer, int period, int baud, int rx_pin, int tx_pin)
{
	ctx->timer 			= timer;
	ctx->period 		= period;
	ctx->baud 			= baud;
	ctx->rx_pin 		= rx_pin;
	ctx->tx_pin 		= tx_pin;
	ctx->isInitialized 	= 0;
	ctx->gsm_serial 	= &gsmSerialGlobal;
}

void loop_GSM(struct GSM_s *ctx)
{
	int ret;

	if(!(waitFor(ctx->timer, ctx->period)))
		return;

	/**
	 * @brief SMS.setup() is called periodically. There is a state machine in the library
	 * that handles the initialization of the component, and it allows to not have 
	 * a blocking function
	 */
	if (SMS.initialized() == 0) {
		if(SMS.setup(ctx->gsm_serial, ctx->baud, ctx->rx_pin, ctx->tx_pin) == 1) {
			ctx->isInitialized = 1;
		}
		return;
	}

	SMS.loop();
	ret = SMS.read(ctx->sender, ctx->data);

	if( ret == 1) {
		Serial.print("SMS recived from: ");
		Serial.print(ctx->sender);
		Serial.print(" : ");
		Serial.println(ctx->data);

		Disp1.curScreen = 3;

		text_command(ctx->data, CMD_SRC_SMS);

	}

	if(!(waitFor(MAX_WAIT_FOR_TIMER-1, 25000000)))
		return;
}


/*********************************************************/
/* Linky protocoles methodes */
/*********************************************************/

void setup_linky(struct Linky_s *ctx, int timer, int period)
{
	ctx->timer 			= timer;
	ctx->period 		= period;
	ctx->linky_serial 	= &linkySerialGlobal;
	ctx->allData 		= "";
	ctx->connected		= 0;
	ctx->current 		= 0;
	ctx->voltage 		= 0;
	ctx->power 			= 0;
	ctx->newData		= 0;
	ctx->newData		= 0;

	//if(LINKY.setup(ctx->linky_serial, 9600, LINKY_RX_PIN, LINKY_TX_PIN) == -1)
		//return;

	//ctx->initialized = 1;
	return;
}

void setup_linky(struct Linky_s *ctx)
{
	if(LINKY.setup(ctx->linky_serial, 9600, LINKY_RX_PIN, LINKY_TX_PIN) == -1)
		return;

	return;
}

void loop_linky(struct Linky_s *ctx)
{
	if (LINKY.isInitialized() == 0) {
		setup_linky(ctx);
		return;
	}
	
	if(!(waitFor(ctx->timer, ctx->period)))
		return;
	
	LINKY.readData();
	
	if (LINKY.dataAvailable()) {
		ctx->allData = LINKY.getAllData();
		ctx->current = LINKY.getCurrent();
		ctx->voltage = LINKY.getVoltage();
		ctx->power   = LINKY.getPower();
		
		/* Complement variable to indicate in the screen when new data available */
		ctx->newData = 1 - ctx->newData;

		/**
		 * @brief Increment index, and check if index is at the end of the array
		 * In that case, restart saving the components at the begeing
		 */
		ctx->statsCurrent.index++;
		ctx->statsPower.index++;
		ctx->statsVoltage.index++;
		if (ctx->statsCurrent.index >= LINKY_MEAN_SIZE)
			ctx->statsCurrent.index = 0;
		if (ctx->statsPower.index >= LINKY_MEAN_SIZE)
			ctx->statsPower.index = 0;
		if (ctx->statsVoltage.index >= LINKY_MEAN_SIZE)
			ctx->statsVoltage.index = 0;

		/* increment number of elemnts only when inferior to max number of elements */
		ctx->statsCurrent.nbElements += (ctx->statsCurrent.nbElements < LINKY_MEAN_SIZE) ? 1 : 0; 
		ctx->statsPower.nbElements += (ctx->statsPower.nbElements < LINKY_MEAN_SIZE) ? 1 : 0; 
		ctx->statsVoltage.nbElements += (ctx->statsVoltage.nbElements < LINKY_MEAN_SIZE) ? 1 : 0; 

		/* Save measuremeent into the array */
		ctx->statsCurrent.measurements[ctx->statsCurrent.index].value = ctx->current;
		ctx->statsPower.measurements[ctx->statsPower.index].value = ctx->power;
		ctx->statsVoltage.measurements[ctx->statsVoltage.index].value = ctx->voltage;

		/* Calculate statistics every 10 meassurement */
		ctx->statsCount++;
		if (ctx->statsCount > 5) {
			statistics(&Linky1.statsCurrent);
			statistics(&Linky1.statsPower);
			statistics(&Linky1.statsVoltage);

			ctx->statsCount = 0;
		}
		
	}

} 

/**********************************************************/
/*                    BLE FUNCTIONS                       */
/**********************************************************/

class MyAdvertisedDeviceCallbacks : public BLEAdvertisedDeviceCallbacks
{
	void onResult(BLEAdvertisedDevice advertisedDevice)
	{	
		
		String deviceName;
		String manufData;

		if (advertisedDevice.haveName()) {
			deviceName = advertisedDevice.getName().c_str();
			if (deviceName.indexOf("noWatt") == -1)
				return;
		} else {
			return;
		}

		if (advertisedDevice.haveManufacturerData() == true) {
			std::string strManufacturerData = advertisedDevice.getManufacturerData();

			uint8_t cManufacturerData[100];
			strManufacturerData.copy((char *)cManufacturerData, strManufacturerData.length(), 0);

			Serial.printf("%s-%d\n", deviceName.c_str(), strManufacturerData.length());
			for (int i = 0; i < strManufacturerData.length(); i++) {
				manufData += (char)cManufacturerData[i];
			}

			Ble1.manufacturerData 	= manufData;
			Ble1.deviceName 		= deviceName;
		}
		return;
	}
};

void setup_BLE(struct BLE_s *ctx, int timer, int period)
{
 	ctx->timer 			= timer;
	ctx->period 		= period;
	ctx->initialized 	= 0;

	Serial.print("Setting up BLE...");

	BLEDevice::init("");
	esp_ble_tx_power_set(ESP_BLE_PWR_TYPE_SCAN, ESP_PWR_LVL_P9);
	ctx->pBLEScan = BLEDevice::getScan(); // create new scan
	ctx->pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
	ctx->pBLEScan->setActiveScan(true); // active scan uses more power, but get results faster
	ctx->pBLEScan->setInterval(25);
	ctx->pBLEScan->setWindow(24); // less or equal setInterval value

	ctx->initialized = 1;

	Serial.println("OK");
}

void loop_BLE(struct BLE_s *ctx)
{
	if(!(waitFor(ctx->timer, ctx->period)))
		return;

	// put your main code here, to run repeatedly:
	BLEScanResults foundDevices = ctx->pBLEScan->start(SCANTIME, false);
	ctx->pBLEScan->clearResults(); // delete results fromBLEScan buffer to release memory
}

/**********************************************************/
/*                  NTP TIME FUNCTIONS                    */
/**********************************************************/

void setup_NTP_Time(struct NTP_Time_s *ctx, int timer, int period)
{
	ctx->timer 			= timer;
	ctx->period 		= period;
	ctx->initialized 	= 0;
	ctx->timeSet		= isTimeSet; 

	Serial.print("Setting up NTPClient Time...");

	timeClient.begin();
	// Set offset time in seconds to adjust for your timezone, for example:
	// GMT +1 = 3600
	// GMT +8 = 28800
	// GMT -1 = -3600
	// GMT 0 = 0
	timeClient.setTimeOffset(7200);

	ctx->initialized = 1;

	Serial.println("OK");
}

void loop_NTP_Time(struct NTP_Time_s *ctx)
{
	/* If NTP client not initialized, call to setup */
	if (ctx->initialized == 0) {
		setup_NTP_Time(ctx, 11, 1000000); //Perios 60s
		return;
	}

	/* Wait for execute periodically */
	if(!(waitFor(ctx->timer, ctx->period)))
		return;

	/* Get time from RTC when timer set */
	if (ctx->timeSet) {
		getLocalTime(&ctx->now, 0);

		/* Get day change */
		if (ctx->currentDay != ctx->now.tm_mday) { //Day changed
			update_new_day_info();
		}

		ctx->currentDay = ctx->now.tm_mday; //day of the month

		return;
	}

	/* Get out of fucntion if not connected */
	if (!Wifi1.connected) {
		return;
	}

	if(!timeClient.update()) {
		timeClient.forceUpdate();
	}

	/* If esp32 dont have the time yet, return */
	if (!timeClient.isTimeSet())
		return;

	/* Flag to not try to get the time again */
	ctx->timeSet = 1;
	isTimeSet = 1;

	/* Set epoch time (linux time) only once */
	timeval epoch = {timeClient.getEpochTime(), 0};
	if (settimeofday((const timeval*)&epoch, 0)) {
		Serial.println("ERROR TIME SET");
	}

	return;
}

/**********************************************************/
/*                  BUZZER FUNCTIONS                      */
/**********************************************************/

void setup_Buzzer(struct Buzzer_s *buzzer, int timer, int period, int pin){
  buzzer->timer   = timer;
  buzzer->period  = period;
  buzzer->pin     = pin;
  buzzer->freq    = 700;
  buzzer->value   = 0;

  //Configuration du signal PWM  Pour buzzer
  ledcAttachPin(buzzer->pin, PWM_BUZZERCHANNEL);
  ledcSetup(PWM_BUZZERCHANNEL, buzzer->freq, PWM_RESOLUTION);
}

void loop_Buzzer(struct Buzzer_s *buzzer){
  /*Static variables, the value is the same at every function call*/
  static int lastVal;
  static int lastFrq;

  /*
   * Check if any message is recived or the time is elapsed
   * 1 -> Message sent from button read function
   * 2 -> Message sent from serial port value recived
   * 3 -> Message sent from MQTT publisher
   */
  if(!(waitFor(buzzer->timer,buzzer->period))) return;
  
  buzzer->value = Button1.value;

  /*Detect and execute the command only if the buzzer val or freq has changed*/
  if(lastVal != buzzer->value or lastFrq != buzzer->freq){
    if(buzzer->value){
      ledcWriteTone(PWM_BUZZERCHANNEL, buzzer->freq);
    }else{
      ledcWriteTone(PWM_BUZZERCHANNEL, 0);
    }

    /*Update Values*/
    lastVal = buzzer->value;
    lastFrq = buzzer->freq;
  }
}

/**********************************************************/
/*                   Setup et Loop                        */
/**********************************************************/

void setup()
{
	Serial.println(" ");
	
	setup_Led		(&Led1, 0, 10000, LED_BUILTIN);                        // Led est exécutée toutes les 100ms 
	setup_Mess		(&Mess1, 1, 1000000, "bonjour: ");  
	setup_Display	(&Disp1, 2, 250*1000);  // Timer 2, period 100ms
	setup_Light		(&Light1, 3, 36, 50*1000); // Timer 3, pin 9, period 10ms
	setup_Button	(&Button1, 5, 5*1000, BUTTON_PIN);
	loop_Display	(&Disp1);
	setup_GSM		(&Gsm1, 6, 500000, 9600, GSM_RX_PIN, GSM_TX_PIN);
	setup_wifi		(&Wifi1, 7, 1000000, ssid, password);
	setup_mqtt		(&Mqtt1, 8, 1000000, mqtt_server, 1883);
	setup_linky   	(&Linky1, 9, 250 * 1000);
	setup_BLE		(&Ble1, 10, 1000);
	setup_Buzzer	(&Buzzer1, 11, 250000, BUZZER_PIN);

	/*Sleep and wake up operations*/
	++bootCount;
	Serial.print("Boot count: ");
	Serial.println(bootCount);
	print_wakeup_reason();

	 /* timerBegin(NoTimer, Prescalar value, count up flag) */
	My_timer = timerBegin(0, 80, true); //FREE RTOS uses timer 0 for 
	timerAttachInterrupt(My_timer, &onTimer, true);
	timerAlarmWrite(My_timer, INTERRUPT_TIMER_TIME, true);
	timerAlarmEnable(My_timer);

	/*Multi Core operations, each core will execure a infinite loop*/
	xTaskCreatePinnedToCore(loop_zero, "coreZero", 10000, NULL, 9, &coreZero, 0);
	delay(500);
	xTaskCreatePinnedToCore(loop_one,  "coreOne",  10000, NULL, 9, &coreOne,  1);
	delay(500);
}

void loop_zero(void *param)
{
	Serial.print("coreZero running on core ");
	Serial.println(xPortGetCoreID());
	/*simulate the classic loop function*/
	while(1) {
		//loop_Led(&Led1, &BoiteLed);                                        
		loop_Mess(&Mess1, &Led1, &Boite1);
		loop_Display(&Disp1);
		loop_Light(&Light1, &Boite1, &BoiteLed);
		loop_Button(&Button1, &BoiteBtn_Bzz);
		//loop_Buzzer(&Buzzer1, &BoiteBtn_Bzz);
		loop_GSM(&Gsm1);
		loop_linky(&Linky1);
		loop_NTP_Time(&Time1);
		loop_mqtt(&Mqtt1);

 		if (Serial.available())
			serialEvent();

		//TEST
		//simulate_TIC();

		//Alarmas
		alarms();
	}
}

void loop_one(void *param)
{
	Serial.print("CoreOne running on core ");
	Serial.println(xPortGetCoreID());
	while(1) {
		//loop_BLE(&Ble1);
		//TEST
		//simulate_TIC();
		touchPressType();
		loop_Buzzer(&Buzzer1);

		if (timerIrqFlag) {
			store_in_historc(&Linky1.historicCurrent, Linky1.statsCurrent.stats.average);
			store_in_historc(&Linky1.historicPower, Linky1.statsPower.stats.average);
			store_in_historc(&Linky1.historicVoltage, Linky1.statsVoltage.stats.average);
			/* Flag must be placed to 0 */
			timerIrqFlag = 0;
		}
	}
}

void loop() {}

/**********************************************************/
/*                   EVENT HANDLERS                       */
/**********************************************************/

/**
 * @brief Called when the Seria object
 * recives any information. Called by the loop function
 * 
 */
void serialEvent(){

	String data;

	delay(50);

	while (Serial.available()){
		data += (char)Serial.read();
	}

	text_command(data, CMD_SRC_SERIAL);

}

/**
 * @brief Handler for MQTT messages communications
 * 
 * @param topic 	Topic recived
 * @param message 	Data of the topic
 * @param length 	Nomber of data bytes
 */
void callback(char* topic, byte* message, unsigned int length) {
	Serial.print("Message arrived on topic: ");
	Serial.print(topic);
	Serial.print(". Message: ");
	String messageTemp;
	
	for (int i = 0; i < length; i++) {
		Serial.print((char)message[i]);
		messageTemp += (char)message[i];
	}
	Serial.println();

	/*
	 *  If a message is received on the topic esp32/buzzerValue, you check if the message is either "1" or "0". 
	 */
	if (String(topic) == "esp32/buzzerValue") {
		Serial.print("Changing output to ");
		Serial.print(messageTemp);
		if(messageTemp == "1"){
			BoiteBtn_Bzz.bData = 1;
			BoiteBtn_Bzz.flag = 3; /*New value from MQTT recived to buzzer*/
		}
		else if(messageTemp == "0"){
			BoiteBtn_Bzz.bData = 0;
			BoiteBtn_Bzz.flag = 3; /*New value from MQTT recived to buzzer*/
		}
	}
}

/**
 * @brief Method to print the reason by which ESP32
 * has been awaken from sleep
 */
void print_wakeup_reason(){
	esp_sleep_wakeup_cause_t wakeup_reason;
	
	wakeup_reason = esp_sleep_get_wakeup_cause();

	switch(wakeup_reason)
	{
		case ESP_SLEEP_WAKEUP_UNDEFINED 		: Serial.println("In case of deep sleep, reset was not caused by exit from deep sleep"); break;
		case ESP_SLEEP_WAKEUP_ALL 				: Serial.println("Not a wakeup cause"); break;
		case ESP_SLEEP_WAKEUP_EXT0 				: Serial.println("Wakeup caused by external signal using RTC_IO"); break;
		case ESP_SLEEP_WAKEUP_EXT1 				: Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
		case ESP_SLEEP_WAKEUP_TIMER 			: Serial.println("Wakeup caused by timer"); break;
		case ESP_SLEEP_WAKEUP_TOUCHPAD 			: Serial.println("Wakeup caused by touchpad"); break;
		case ESP_SLEEP_WAKEUP_ULP 				: Serial.println("Wakeup caused by ULP program"); break;
		case ESP_SLEEP_WAKEUP_GPIO 				: Serial.println("Wakeup caused by GPIO (light sleep only) "); break;
		case ESP_SLEEP_WAKEUP_UART 				: Serial.println("Wakeup caused by UART (light sleep only) "); break;
		case ESP_SLEEP_WAKEUP_WIFI 				: Serial.println("Wakeup caused by WIFI (light sleep only) "); break;
		case ESP_SLEEP_WAKEUP_COCPU 			: Serial.println("Wakeup caused by COCPU int. "); break;
		case ESP_SLEEP_WAKEUP_COCPU_TRAP_TRIG	: Serial.println("Wakeup caused by COCPU crash. "); break;
		case ESP_SLEEP_WAKEUP_BT 				: Serial.println("Wakeup caused by BT (light sleep only) "); break;
		default : Serial.printf("First Wakeup, reason: %d\n",wakeup_reason); break;
	}
}

/**
 * @brief WIFI event handler, every event will call
 * this function
 * 
 * @param event 
 */
void WiFiEvent(WiFiEvent_t event) {
	//Serial.printf("[WiFi-event] event: %d\n", event);

	switch (event) {
		case SYSTEM_EVENT_WIFI_READY: 
			Serial.println("WiFi interface ready");
			break;
		case SYSTEM_EVENT_SCAN_DONE:
			Serial.println("Completed scan for access points");
			break;
		case SYSTEM_EVENT_STA_START:
			Serial.println("WiFi client started");
			break;
		case SYSTEM_EVENT_STA_STOP:
			Serial.println("WiFi clients stopped");
			break;
		case SYSTEM_EVENT_STA_CONNECTED:
			Serial.println("Connected to access point"); //
			break;
		case SYSTEM_EVENT_STA_DISCONNECTED: //Disconnected from a network
			//Serial.println("Disconnected from WiFi access point");
			//Serial.println("");
			Wifi1.connected = 0;
			WiFi.begin(Wifi1.ssid, Wifi1.pass);
			break;
		case SYSTEM_EVENT_STA_AUTHMODE_CHANGE:
			Serial.println("Authentication mode of access point has changed");
			break;
		/*CASE ALREADY HANDLED BY THE NEXT FUNCTION*/
		case SYSTEM_EVENT_STA_GOT_IP:
		  Serial.print("Obtained IP address: "); 
		  Serial.println(WiFi.localIP());
		  Wifi1.connected = 1;
		  break;
		case SYSTEM_EVENT_STA_LOST_IP:
			Serial.println("Lost IP address and IP address is reset to 0");
			break;
		case SYSTEM_EVENT_STA_WPS_ER_SUCCESS:
			Serial.println("WiFi Protected Setup (WPS): succeeded in enrollee mode");
			break;
		case SYSTEM_EVENT_STA_WPS_ER_FAILED:
			Serial.println("WiFi Protected Setup (WPS): failed in enrollee mode");
			break;
		case SYSTEM_EVENT_STA_WPS_ER_TIMEOUT:
			Serial.println("WiFi Protected Setup (WPS): timeout in enrollee mode");
			break;
		case SYSTEM_EVENT_STA_WPS_ER_PIN:
			Serial.println("WiFi Protected Setup (WPS): pin code in enrollee mode");
			break;
		case SYSTEM_EVENT_AP_START:
			Serial.println("WiFi access point started");
			break;
		case SYSTEM_EVENT_AP_STOP:
			Serial.println("WiFi access point  stopped");
			break;
		case SYSTEM_EVENT_AP_STACONNECTED:
			Serial.println("Client connected");
			break;
		case SYSTEM_EVENT_AP_STADISCONNECTED:
			Serial.println("Client disconnected");
			break;
		case SYSTEM_EVENT_AP_STAIPASSIGNED:
			Serial.println("Assigned IP address to client");
			break;
		case SYSTEM_EVENT_AP_PROBEREQRECVED:
			Serial.println("Received probe request");
			break;
		case SYSTEM_EVENT_GOT_IP6:
			Serial.println("IPv6 is preferred");
			break;
		case SYSTEM_EVENT_ETH_START:
			Serial.println("Ethernet started");
			break;
		case SYSTEM_EVENT_ETH_STOP:
			Serial.println("Ethernet stopped");
			break;
		case SYSTEM_EVENT_ETH_CONNECTED:
			Serial.println("Ethernet connected");
			break;
		case SYSTEM_EVENT_ETH_DISCONNECTED:
			Serial.println("Ethernet disconnected");
			break;
		case SYSTEM_EVENT_ETH_GOT_IP:
			Serial.println("Obtained IP address");
			break;
		default: break;
	}
}

/**
 * @brief Handler for Wifi connection at the moment when an IP is
 * assigned by the acces point to the ESP32
 * 
 * @param event 
 * @param info 
 */
/* void WiFiGotIP(WiFiEvent_t event, WiFiEventInfo_t info)
{
		Serial.println("WiFi connected");
		Serial.println("IP address: ");
		Serial.println(IPAddress(info.got_ip.ip_info.ip.addr));
		Wifi1.connected = 1;
} */

void IRAM_ATTR onTimer(){
	timerIrqFlag++;
}
