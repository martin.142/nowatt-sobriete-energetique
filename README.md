sd# noWatt - Sobriété Énergétique

Internshitp at LIP6 Laboratory in Sorbonne University. June - August 2022<br />
Master's degree, SESI

![Main diagram](./img/LIP6-big-transparent.png "Lip6 Logo")

Project done by: Martin Eduardo ACOSTA CORRAL <br />
Supervisor: Franck WAJSBÜRT

[Overleaf Rapport de stage](https://www.overleaf.com/read/zttwwyzhtfvc) <br />
[Overleaf Rapport technique](https://www.overleaf.com/read/hrsfzmjgngwv)

# Project description

This project is intended to develop a complete systems capable of reducing and controling the energy consumption in home buildings. By studing the embedded systems and its technologies such as Bluetooth Low Energy, BEACON devices, GSM, embedded protocols, sleep modes, etc. we'll create a bunch of devices that could work together with the main objective of measure and control the consumption in a home.

The main objective of this project is to study the energy usage in home building, how can we use the embedded systems to reduce this energy consumption and finally the real impact of adding components thas use energy to save energy. Personally, it will help me to develp my skill in embedded protocols, wireless communication, electronics and embedded software developement and in general about electronics.

**A REVSER** Poposé par M. Frank, cela consiste à crée une station de base qui sera connecté au compteur linky de la maison (pas pour toutes les cas), l'idée est de lire les données via le port serie et faire des statistiques avec l'objet de prevenir l'utilisauter de leur consomation, alertes, etc. C'est-à-dire que si jamais il existe une consomation anormal par rapport à un certaint historique.

Initial proposition by M. Frank. The project consists in creating a base stations that will be directlry connected to the Linky counter. The base station will obtain the information out of the TIC port in order to do stats with these data and try to reduce power usaged at home.

Every sub-project is preceded by noWatt. It's part of the same project.

# Project organization

Most part of the internship foucused in the **base_station** directory, this part contains the main code and uses differentes embedded technologies. There are also other folders that indicates mainly *sub-stations*  such a a wireless temperature sensor, or a connected electrical outlet. Those components controled by the main station, by the user via wifi/bluetooth in a phone application (in developement) or manually would create a complet array of connected devices.

- noWatt_base_station : Main station composed by a esp32, gsm module, light sensor, boutton, display, wifi, mqtt client, beacon scanner. 
- noWatt_beaconTest : esp32-c3 module that sends basic data periodically to the base station. Its main objective is to communicate using very low energyd
- noWatt_connected_outlet : esp32-c3 (to define) module that uses a seres of relays to connect and disconnect the power supply of a ceraint component when there is no need for it to be connected.


# Resources / Useful information

ESP32 information : https://lastminuteengineers.com/electronics/esp32-projects/
Information lecture compteur linky via son port serie: https://github.com/MediaTek-Labs/linkIt-ONE-additional-example, 

Project lecture donnnées du competeur linky avec raspberry pi zero : https://www.jonathandupre.fr/articles/24-logiciel-scripts/208-suivi-consommation-electrique-compteur-edf-linky-avec-raspberry-pi-zero-w/

Details sur les informations envoyées par le port serie du competeur linky : https://lucidar.me/fr/home-automation/linky-customer-tele-information/

Detail sur le protocole linky : https://www.yadnet.com/domotique/protocole-teleinfo-du-compteur-linky/

Example des données sortie du protocle teleinfo du compteur linky en mode historique et stardard: https://blog.bigd.fr/suivre-sa-consommation-electrique/

Linky et esp32 project : https://github.com/paulgreg/esp32-linky-epaper

Lien amazon carte SIM800L (Carte Rouge) -> 11 Euros : https://www.amazon.fr/Sim800l-Quadribande-Embarqu%C3%A9e-Antenne-Arduino/dp/B01FQPLG9W
    Details : https://lastminuteengineers.com/sim800l-gsm-module-arduino-tutorial/
    Fabricante : https://www.simcom.com/product/SIM800.html

Lien amazon Carte SIM800L V2.0 (Carte Blue) -> 17 Euros : https://www.amazon.fr/SIM800L-Wireless-Quad-Band-Antenna-Quadband/dp/B07LH3PLB2/ref=pd_lpo_1?pd_rd_i=B07LH3PLB2&psc=1

Use three serial connections : https://hackaday.com/2017/08/17/secret-serial-port-for-arduinoesp32/

SEND RECIVE MESSAGES : https://www.developershome.com/sms/cmgrCommand2.asp

info SIM800L : https://circuitdigest.com/microcontroller-projects/interfacing-sim800l-module-with-esp32

Valeurs de résistances : 147 ohms et 388 ohms

## Étude energie

Information et chiffres clées sur la consomation énergétique en France : https://www.statistiques.developpement-durable.gouv.fr/chiffres-cles-de-lenergie-edition-2021#:~:text=L'%C3%A9nergie%20p%C3%A8se%20%C3%A0%20hauteur,approvisionnement%20en%20%C3%A9nergie%20du%20territoire.

